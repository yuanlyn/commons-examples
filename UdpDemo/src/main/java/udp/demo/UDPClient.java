package udp.demo;


/*客户端*/
import java.net.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author liuzhenzhao
 */
public class UDPClient {

    /**
     * @param args the command line arguments
     */
    private final static int PORT=3000;//服务器的端口号
    private final static String IP="172.16.7.200";///服务器的ip地址
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            DatagramSocket socket=new DatagramSocket(0);//指定0让操作系统指定一个端口号
            socket.setSoTimeout(10000);//定义超时时间
            InetAddress ip=InetAddress.getByName(IP);
            byte []sendbuff="hello".getBytes();
            System.out.println(sendbuff.length);
            DatagramPacket send=new DatagramPacket(sendbuff,sendbuff.length,ip,PORT);//用于发送的包
            DatagramPacket recv=new DatagramPacket(new byte[10],10);//用于接收的包
            socket.send(send);
            System.out.println("send message already");
            socket.receive(recv);
            String res=new String(recv.getData(),0,recv.getLength(),"ASCII");
            System.out.println("has recv:"+res);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
            System.err.println(ex);
        }
    }
}