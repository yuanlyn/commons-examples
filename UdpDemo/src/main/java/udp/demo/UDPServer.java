package udp.demo;
/*服务器端*/
import java.util.*;
import java.net.*;
import java.io.*;
public class UDPServer {
    private final static int PORT=3000;
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            DatagramSocket socket=new DatagramSocket(PORT);
            while(true)
            {
                try{
                    DatagramPacket hasRecv=new DatagramPacket(new byte[10],10);
                    socket.receive(hasRecv);
                    String str=new String(hasRecv.getData(),0,hasRecv.getLength(),"ASCII");
                    System.out.println("has recv:"+str);
                    byte[]data="ACK".getBytes();
                    DatagramPacket toSend=new DatagramPacket(data,data.length,hasRecv.getAddress(),hasRecv.getPort());
                    socket.send(toSend);

                }
                catch(IOException ex)
                {
                    System.err.println(ex);
                }
            }
        }
        catch(IOException ex)
        {
            System.err.println(ex);
        }
    }
}


