package udp.demo.broadcast;

import java.io.IOException;
import java.net.*;

public class UdpBroadcastServer {

    public static void main(String[] args) {

        int port = 9999;// 开启监听的端口
        DatagramSocket ds = null;
        DatagramPacket dp = null;
        byte[] buf = new byte[1024];// 存储发来的消息
        try {
            // 绑定端口的
            ds = new DatagramSocket(port);
            dp = new DatagramPacket(buf, buf.length);
            System.out.println("监听广播端口打开：");
            while (true) {
                ds.receive(dp);
                int i;
                StringBuffer sbuf = new StringBuffer();
                for (i = 0; i < 1024; i++) {
                    if (buf[i] == 0) {
                        break;
                    }
                    sbuf.append((char) buf[i]);
                }
                System.out.println("收到广播消息：" + sbuf.toString());

                byte[]data="ACK".getBytes();
                DatagramPacket toSend=new DatagramPacket(data,data.length,dp.getAddress(),dp.getPort());
                ds.send(toSend);
            }
        }
        catch (SocketException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}