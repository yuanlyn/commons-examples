package udp.broadcast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by yl on 2019/12/9
 */
public class udp_server2 {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket(8888,InetAddress.getByName("255.255.255.255"));

        byte bytes[] = new byte[1024];
        DatagramPacket datagramPacket = new DatagramPacket(bytes,1024);

        datagramSocket.receive(datagramPacket);
        System.out.println(new String(datagramPacket.getData(),"utf-8"));

        datagramSocket.close();
    }
}
