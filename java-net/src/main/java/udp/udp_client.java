package udp;

import java.io.IOException;
import java.net.*;

/**
 * Created by yl on 2019/12/9
 */
public class udp_client {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket();
        byte message[] = "hello world".getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(message,
                0,
                message.length,
                InetAddress.getByName("127.0.0.1"),
                8888);
        datagramSocket.send(datagramPacket);
        datagramSocket.close();
    }
}
