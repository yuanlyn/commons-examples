package udp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by yl on 2019/12/9
 */
public class udp_server {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket(8888);

        byte bytes[] = new byte[1024];
        DatagramPacket datagramPacket = new DatagramPacket(bytes,1024);

        datagramSocket.receive(datagramPacket);
        System.out.println(new String(datagramPacket.getData(),"utf-8"));

        datagramSocket.close();
    }
}
