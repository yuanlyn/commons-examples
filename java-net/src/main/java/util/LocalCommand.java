package util;

import cn.hutool.core.util.RuntimeUtil;

import java.util.ArrayList;
import java.util.List;

public class LocalCommand {

    public static String[] exec(String commnad){
        ArrayList<String> commandList = new ArrayList<>();
        commandList.add("sh");
        commandList.add("-c");
        commandList.add(commnad);
        String s = RuntimeUtil.execForStr(commandList.toArray(new String[commandList.size()]));
        String[] split = s.split("\n");
        return  split;
    }

    public static void main(String[] args) {
        String command = "/home/hadoop/echoy.sh";
        String[] exec = exec(command);
        for (String s : exec) {
            System.out.println(s);
        }
    }
}
