package util;

import cn.hutool.core.util.RuntimeUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GatewayListUtil {
    public static List<String> getGatewayList(){
        String command =  "route -n|awk '{print$1}'";
        //String command = "hostname";

        ArrayList<String> commandList = new ArrayList<>();
        commandList.add("sh");
        commandList.add("-c");
        commandList.add("route -n|grep -v Gateway|grep -v routing|awk '{print$2}'");
        String str = RuntimeUtil.execForStr(commandList.toArray(new String[commandList.size()]));

//        System.out.println(str);
        String[] split = str.split("\n");

        List<String> collect = Arrays.stream(split).filter(s -> {
            return s.matches("[1-9]\\d{0,2}.[1-9]\\d{0,2}.[1-9]\\d{0,2}.[1-9]\\d{0,2}");
        }).collect(Collectors.toList());
        collect.forEach(System.out::println);
        return collect;
    }

    public static void main(String[] args) {
        getGatewayList();
    }
}
