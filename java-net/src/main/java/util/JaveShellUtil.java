package util;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Locale;

public class JaveShellUtil {

    public static int execCommand(String command) {
        int retCode = 0;
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", command}, null, null);

            process.getOutputStream().write("john".toString().getBytes());
            process.getOutputStream().flush();
            execOutput(process);
            execOutputError(process);
            retCode = process.waitFor();

        } catch (Exception e) {
            retCode = -1;
        }
        return retCode;
    }

    public static boolean execOutput(Process process) throws Exception {
        if (process == null) {
            return false;
        } else {
            InputStreamReader ir = new InputStreamReader(process.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            String line;
            String output = "";
            while ((line = input.readLine()) != null) {
                output += line + "\n";
            }
            input.close();
            ir.close();
            if (output.length() > 0) {
                System.out.println(output);
            }
        }
        return true;
    }


    public static boolean execOutputError(Process process) throws Exception {
        if (process == null) {
            return false;
        } else {
            InputStreamReader ir = new InputStreamReader(process.getErrorStream());
            LineNumberReader input = new LineNumberReader(ir);
            String line;
            String output = "";
            while ((line = input.readLine()) != null) {
                output += line + "\n";
            }
            input.close();
            ir.close();
            if (output.length() > 0) {
                System.out.println(output);
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int ll = execCommand("/home/hadoop/echoy.sh");
        System.out.println(ll);
    }
}