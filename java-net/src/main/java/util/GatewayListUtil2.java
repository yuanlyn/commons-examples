package util;

import cn.hutool.core.util.RuntimeUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GatewayListUtil2 {
    public static String getGatewayList(){
        String command =  "ip route|grep -i default";
        //String command = "hostname";

        ArrayList<String> commandList = new ArrayList<>();
        commandList.add("sh");
        commandList.add("-c");
        commandList.add(command);
        String str = RuntimeUtil.execForStr(commandList.toArray(new String[commandList.size()]));




        Pattern compile = Pattern.compile("([1-9]\\d{0,2}.[1-9]\\d{0,2}.[1-9]\\d{0,2}.[1-9]\\d{0,2})");
        Matcher matcher = compile.matcher("default via 192.168.150.2 dev ens33 proto static metric 100");
        if (matcher.find()){
            System.out.println(matcher.group(1));
        }


        return null;
    }

    public static void main(String[] args) {
        getGatewayList();
    }
}
