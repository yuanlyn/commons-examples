package com.company.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.bean.User;

public interface UserMapper extends BaseMapper<User> {

}
