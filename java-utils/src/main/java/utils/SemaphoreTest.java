package utils;

import java.util.concurrent.Semaphore;

public class SemaphoreTest {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(10);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    semaphore.release(2);
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
        while (true){

            try {
                semaphore.acquire();
                System.out.println("got candy");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
