package utils;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {
    public static void main(String[] args) {
        final CountDownLatch countDownLatch = new CountDownLatch(10);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    countDownLatch.countDown();
                    try {
                        Thread.currentThread().sleep(500);//模拟一个手残党
                        System.out.println("hard working ...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
        System.out.println("i'm main,i am waiting,please hurry up !");
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("finally done!");
    }
}
