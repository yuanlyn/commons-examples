package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsFill {


    public static void main(String[] args) {
        //这将不会往list中添加任何东西，list的长度仍然是0
        List<String> list = new ArrayList<>(4);
        list.add("b");
        Collections.fill(list,"a");

        list.forEach(System.out::println);

        List<String> list2 = new ArrayList(2){
            {
                add(null);
                add(null);
                add(null);
                add(null);
            }
        };
        System.out.println(list2.get(0));
        list2.forEach(System.out::println);
    }
}
