package utils;

import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RetainAllList {

    public static void main(String[] args) {
        HashSet<String> list1 = new HashSet<>();
        list1.add("aaa");
        list1.add("bbb");
        list1.add("ccc");
        HashSet<String> list2 = new HashSet<>();
        list2.add("ccc");
        list2.add("ddd");
        list2.add("fff");
//        list1.forEach(System.out::println);
//        list2.forEach(System.out::println);

        list1.retainAll(list2);
        list1.forEach(System.out::println);
        list2.forEach(System.out::println);
        Sets.SetView<String> res = Sets.intersection(list1, list2);
        res.forEach(System.out::println);

    }
}
