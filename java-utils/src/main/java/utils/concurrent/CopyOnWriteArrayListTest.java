package utils.concurrent;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class CopyOnWriteArrayListTest {
    public static void main(String[] args) {
        //ArrayList list = new ArrayList();
        CopyOnWriteArrayList list = new CopyOnWriteArrayList();
        for (int i = 0; i < 10; i++) {
            list.add((char)(i+65));
        }
        final Iterator iterator = list.iterator();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (iterator.hasNext()){
                    System.out.print(iterator.next());
                }
            }
        });
        thread.start();

        list.add("abc");
        list.set(8,"abc");

//        list.remove(1);
//        list.remove(2);
//        list.remove(3);
//        list.remove(4);
//        list.remove(5);
//        list.remove(6);
//        list.remove(7);

        //类似于snapshot，
        //在新增或者修改的时候，不会出现ConcurrentModificationException,也不会消费新增的数据
        //但是在remove的时候会报错ArrayIndexOutOfBoundsException
    }
}
