package utils;

import java.util.ArrayList;

public class ArrayListTest {
    public static void main(String[] args) {
//        ArrayList a = new ArrayList(4);
//        a.add(1);
        /**
         * 如果new ArrayList不指定初始长度，那么在第一次add的时候，内部的数组就会扩展到10
         * 当内部的数组elementData用尽，会以50%的增幅增加
         * int newCapacity = oldCapacity + (oldCapacity >> 1);
         * ArrayList 特性：就是个增强数组，每次都往size+1的位置放数据，任何数据都可以放，包括null
         * 遍历它也会按照初始顺序来，没有自动排序能力
         */

        ArrayList<Object> objects = new ArrayList<>(4);
        objects.add(1);
        objects.add(2);
        objects.add(3);
        objects.add(4);
        objects.remove(1);
        objects.size();
        System.gc();
        objects.size();
        /**
         * 本意是测试remove方法中elementData[--size] = null;// clear to let GC do its work 的作用，elementData长度仍然是4，不知道gc了啥资源
         */
    }
}

