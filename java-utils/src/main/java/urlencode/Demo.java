package urlencode;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Demo {
    public static void main(String[] args) {
        try {
           String result =  URLDecoder.decode("123%2Babc","utf-8");
            System.out.println(result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
