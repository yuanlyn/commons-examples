package string;
/**
 * Created by yl on 2020/01/07
 */
public class CharUtils {
    public static int getCharFirstPosition(String target,char c){
        char[] chars = target.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if(chars[i] == c){
                return i;
            }
        }
        return -1;
    }

    /**
     * 是否包含？ 和 ?
     * @param target
     * @return
     */
    public static boolean containMark_63_65311(String target){
        if(getCharFirstPosition(target,(char)63) >= 0){
            return true;
        }
        if(getCharFirstPosition(target,(char)65311) >= 0){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        String s1 = "emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文emp_中文_?_？";
        /*for (int i = 0; i < s1.toCharArray().length; i++) {
            System.out.print((int)s1.toCharArray()[i]);
            System.out.print("  ");
        }*/
        long lnow = System.nanoTime();
//        System.out.println(s1.contains("?"));
        s1.contains("?");
        System.out.println(System.nanoTime() - lnow);

        lnow = System.nanoTime();
//        System.out.println(containMark_63_65311(s1));
        containMark_63_65311(s1);
        System.out.println(System.nanoTime() - lnow);

    }
}
