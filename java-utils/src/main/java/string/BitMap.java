package string;

/**
 * 通过int[]实现一个位图
 */
public class BitMap {
    private static int ADDRESS_BITS_PER_WORDS = 5;//因为一个int是32位，也就是2^5，左移五位正好
    int[] words;
    int size;
    public BitMap(int size){
        if(size <0){
            throw new IllegalArgumentException("size can not be negative");
        }
        words = new int[wordsIndex(size)+1];
        this.size = size;
    }

    private int wordsIndex(int size) {
        return size >>> ADDRESS_BITS_PER_WORDS;
    }
    public int size(){
        return size;
    }
    public boolean get(int index){
        return (words[wordsIndex(index)] & (1 << (index-1))) != 0;
    }
    public void set(int index){
        if(index <= 0 || index > size){
            throw new ArrayIndexOutOfBoundsException("max index is "+(size-1));
        }

        words[wordsIndex(index)] |= (1 << (index-1));
    }
}
