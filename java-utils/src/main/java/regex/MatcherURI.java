package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherURI {
    public static void main(String[] args) {
        //正则表达式，匹配一个uri的各个部分
        String hdfs = "AdFs://a:1";
        Pattern pattern = Pattern.compile("([a-z,A-Z]+):\\/\\/(.+):(\\d{1,5})$");
        Matcher matcher = pattern.matcher(hdfs);
//        System.out.println(matcher.find());
        System.out.println(matcher.matches());
        System.out.println(matcher.matches());
        System.out.println(matcher.group(0));
        System.out.println(matcher.group(1));
        System.out.println(matcher.group(2));
        System.out.println(matcher.group(3));
    }
}
