package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
设法匹配restful风格的url
 */
public class MatcherRestfulUrl {
    public static void main(String[] args) {
//        test1();
//        test2();
//        test3();
        test4();
    }

    private static void test1() {
        //        String url = "http://localhost:8080/user/{userid}/delete/{size}";
        String url = "http://localhost:8080/user/31/delete/10";
        Pattern pattern = Pattern.compile("http://localhost:8080/user/.+/delete/.+");
        Matcher matcher = pattern.matcher(url);
        System.out.println(matcher.matches());
    }
    private static void test2() {
        String url = "http://localhost:8080/user/31/delete/10";
        Pattern pattern = Pattern.compile("http://localhost:8080/user/31/delete/10");
        Matcher matcher = pattern.matcher(url);
        System.out.println(matcher.matches());
    }

    private static void test3() {
        String url = "http://localhost:8080/user/31/delete/10:post";
        Pattern pattern = Pattern.compile("http://localhost:8080/user/.+/delete/.+:.+");
        Matcher matcher = pattern.matcher(url);
        System.out.println(matcher.matches());
    }
    private static void test4() {
        String url = "http://localhost:8080/user/{userid}/delete/{size}";
        url = url.replaceAll("\\{.+?\\}",".+");
        System.out.println(url);
    }

}
