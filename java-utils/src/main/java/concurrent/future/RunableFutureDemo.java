package concurrent.future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class RunableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

//        runableFuture();
//        callableFuture();
        runableFutureMap();
    }
    static String result= "this is result";
    private static void runableFuture() throws InterruptedException, ExecutionException {

        FutureTask futureTask = new FutureTask(new Runnable() {
            @Override
            public void run() {
                result = "this is new result";
            }
        },result);
        new Thread(futureTask).start();
        System.out.println(futureTask.isDone());
        System.out.println(futureTask.get());
        System.out.println(futureTask.isDone());
        System.out.println(result);
    }

    private static void runableFutureMap() throws InterruptedException, ExecutionException {
        /*ThreadLocal<String> threadLocal = new ThreadLocal<>();
        FutureTask futureTask = new FutureTask(new Runnable() {
            @Override
            public void run() {
                threadLocal.set("this is new result");
            }
        },result);
        Thread t = new Thread(futureTask);
        t.start();
        System.out.println(futureTask.isDone());
        System.out.println(futureTask.get());
        System.out.println(futureTask.isDone());
        System.out.println(result);
        System.out.println(threadLocal.);*/
    }

    private static void callableFuture() throws ExecutionException, InterruptedException {
        FutureTask task = new FutureTask(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "call success";
            }
        });
        new Thread(task).start();
        System.out.println(task.isDone());
        System.out.println(task.get());
        System.out.println(task.isDone());

        new Thread().start();
    }
}
