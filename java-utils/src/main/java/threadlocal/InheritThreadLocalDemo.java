package threadlocal;

import java.util.concurrent.CountDownLatch;

public class InheritThreadLocalDemo {
    public static void main(String[] args) throws InterruptedException {
        InheritThreadLocalDemo demo = new InheritThreadLocalDemo();
        demo.run();
    }
    InheritableThreadLocal<Integer> threadLocal = new InheritableThreadLocal<>();
    private void run() throws InterruptedException {
        threadLocal.set(1);
        System.out.println("base : " + threadLocal.get());
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new Thread(() -> {
//            threadLocal.set(2);
            System.out.println(threadLocal.get());
            countDownLatch.countDown();
        }).start();
        countDownLatch.await();
        System.out.println("base : " + threadLocal.get());
    }
}
