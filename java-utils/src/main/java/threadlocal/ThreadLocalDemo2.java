package threadlocal;

import java.util.concurrent.CountDownLatch;

public class ThreadLocalDemo2 {
    public static void main(String[] args) throws InterruptedException {

        ThreadLocalDemo2 threadLocalDemo2 = new ThreadLocalDemo2();
        threadLocalDemo2.run();

    }
    ThreadLocal<Object> threadLocal = ThreadLocal.withInitial(() -> {
        return new Object();
    });

    private void run() throws InterruptedException {
//        System.out.println(threadLocal.get());
//        threadLocal.set(2);
        java.util.concurrent.CountDownLatch countDownLatch = new CountDownLatch(10);
        System.out.println("base: " + threadLocal.get().hashCode());
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                if(threadLocal == null){
                    System.out.println("null");
                } else {
                    System.out.println( threadLocal.get().hashCode());
                }
                countDownLatch.countDown();
            }).start();
        }
        threadLocal.remove();
        System.out.println(threadLocal);
        System.gc();

        countDownLatch.await();
        System.out.println(threadLocal);
        System.out.println(threadLocal.get().hashCode());

//        threadLocal.set(2);
//        System.out.println("base: " + threadLocal.get());

    }
}
