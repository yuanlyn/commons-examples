package it;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 遍历器的删除
 */
public class IteratorRemoveTest {

    public static void main(String[] args) {
        List<String> lis = new ArrayList<>();
        lis.add("a");
        lis.add("aa");
        lis.add("aaa");
        lis.add("aaaa");
        lis.add("aaaaa");

        Iterator<String> it = lis.iterator();
        while (it.hasNext()){
            String s = it.next();
            if(s.length() > 2){
                it.remove();
            }
        }
        lis.forEach(System.out::println);

    }
}
