package com.demo;

import java.util.Random;

public class EscapeAnalysis {
    static class Foo{
        public static int counter;
        int x;
        public Foo(){
            x = (counter++);

        }
    }
    public static void main(String[] args) {
        long lNow = System.nanoTime();
        for (int i = 0; i < 10000*10000*10; i++) {
            new Thread(){
                public void run(){
                    Foo foo = new Foo();
                }
            }.start();
        }
        System.out.println("spend : "+(System.nanoTime()-lNow)/1000/1000 + "ms");
    }
}

/*
-server -verbose:gc -XX:+DoEscapeAnalysis
开启逃逸分析后，gc信息输出几行后就停止输出，说明Foo的实例没有逃逸到堆上

-server -verbose:gc -XX:-DoEscapeAnalysis
关闭逃逸分析后gc信息一直在打印，通过jconsole发现堆内存使用情况波动比较大
 */