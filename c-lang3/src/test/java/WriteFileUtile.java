import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by yl on 2019/11/14
 */
public class WriteFileUtile {

    @Test
    public void writefile() throws IOException {
        FileWriter fileWriter = new FileWriter(new File("d:\\data\\data2"));
        char[] chars = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for (int i = 0; i < 10000; i++) {
            fileWriter.write(i+","+ chars[i % 26]);
            fileWriter.write("\n");
        }
        fileWriter.flush();
        fileWriter.close();
    }

    @Test
    public void writefile3() throws IOException {
        FileWriter fileWriter = new FileWriter(new File("d:\\data\\data3"));
        char[] chars = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for (int i = 10000; i < 20000; i++) {
            fileWriter.write(i+","+ chars[i % 26]);
            fileWriter.write("\n");
        }
        fileWriter.flush();
        fileWriter.close();
    }

    @Test
    public void writefile4() throws IOException {
        FileWriter fileWriter = new FileWriter(new File("d:\\data\\data4"));
        char[] chars = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for (int i = 20000; i < 30000; i++) {
            fileWriter.write(i+","+ chars[i % 26]);
            fileWriter.write("\n");
        }
        fileWriter.flush();
        fileWriter.close();
    }
}
