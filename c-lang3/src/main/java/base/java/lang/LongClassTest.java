package base.java.lang;

/**
 * 查看Long类型中缓存对于 ==号的影响
 */
public class LongClassTest {


    public static void main(String[] args) {
        Long a1 = 10000000L;
        Long a2 = 10000000L;
        System.out.println(a1.equals(a2));

        System.out.println(a1 > a2);
        System.out.println(a1 >= a2);

        System.out.println(a1 != a2);
        System.out.println(a1 == a2);
    }


}
