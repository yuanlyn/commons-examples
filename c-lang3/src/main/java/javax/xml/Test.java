package javax.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(Boy.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");// //编码格式
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);// 是否格式化生成的xml串
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, false);// 是否省略xm头声明信息

            Boy boy = new Boy();
            boy.setName("lilei");
            boy.setAge(21);
            List<Boy> friends = new ArrayList<>();
            Boy f1 = new Boy();
            f1.setName("sam");
            f1.setAge(22);
            friends.add(f1);
            Boy f2 = new Boy();
            f2.setAge(24);
            f2.setName("tom");
            friends.add(f2);
            boy.setFriends(friends);

            marshaller.marshal(boy,System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
}
