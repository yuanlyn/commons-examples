package javax.xml;


import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@XmlRootElement(name = "boy")
@XmlType(propOrder = {"name","age","friends"})
public class Boy {
    private String name;

    private Integer age;


    private List<Boy> friends;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    @XmlElementWrapper(name = "friends")
    @XmlElement(name = "boy")
    public List<Boy> getFriends() {
        return friends;
    }

    public void setFriends(List<Boy> friends) {
        this.friends = friends;
    }
}
