package org.apache.lang3.example.annotationUtils;

import org.apache.lang3.example.example;

import java.util.Arrays;

/**
 * Created by yl on 2019/9/27
 */


public class AnnotationUtilsExample implements example {
    public void learn() {

        Class targetClass = MyAnnotationUser.class;
        Arrays.stream(targetClass.getMethods()).forEach(method -> {
            Arrays.stream(method.getDeclaredAnnotations()).forEach(annotation -> {
                System.out.println(annotation.annotationType().getName());
            });
        });
    }

    public static void main(String[] args) {
        AnnotationUtilsExample annotationUtilsExample = new AnnotationUtilsExample();
        annotationUtilsExample.learn();
    }
}
