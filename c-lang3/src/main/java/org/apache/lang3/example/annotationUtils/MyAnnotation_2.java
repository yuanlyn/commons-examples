package org.apache.lang3.example.annotationUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by yl on 2019/9/27
 */
@Target(ElementType.METHOD)
public @interface MyAnnotation_2 {
    String name() default "MyAnnotation_2";
}
