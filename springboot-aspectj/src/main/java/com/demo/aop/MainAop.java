package com.demo.aop;

import org.springframework.aop.framework.ProxyFactory;

public class MainAop {
    public static void main(String[] args) {
        FooImpl foo = new FooImpl();
        FooMethodBeforeAdvice fooMethodBeforeAdvice = new FooMethodBeforeAdvice();
        FooMethodAfterAdvice fooMethodAfterAdvice = new FooMethodAfterAdvice();

        ProxyFactory proxyFactory = new ProxyFactory(foo);
        proxyFactory.addAdvice(fooMethodBeforeAdvice);
        proxyFactory.addAdvice(fooMethodAfterAdvice);

        FooInterface foo2 = (FooInterface) proxyFactory.getProxy();
        foo2.print();
    }
}
