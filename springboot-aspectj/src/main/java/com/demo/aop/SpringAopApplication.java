package com.demo.aop;


import com.sun.javafx.fxml.BeanAdapter;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringAopApplication {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext
                = new ClassPathXmlApplicationContext("bean.xml");
        FooInterface bean = (FooInterface) classPathXmlApplicationContext.getBean("proxyFactoryBean");
        bean.print();

        ProxyFactoryBean proxyFactoryBean = classPathXmlApplicationContext.getBean(ProxyFactoryBean.class);
        Advisor[] advisors = proxyFactoryBean.getAdvisors();
        for (Advisor advisor : advisors) {
            System.out.println(advisor.getAdvice().toString());
        }
        for (Class<?> proxiedInterface : proxyFactoryBean.getProxiedInterfaces()) {
            System.out.println(proxiedInterface.getName());
        }
        System.out.println(proxyFactoryBean.getTargetClass().getName());
    }

//    @Bean
    ProxyFactoryBean getProxyFactoryBean(ProxyFactoryBean bean){
//        ProxyFactoryBean proxyFactoryBean = new ProxyFactoryBean();
//        proxyFactoryBean.setInterceptorNames("FooMethodBeforeAdvice","FooMethodAfterAdvice");
//        proxyFactoryBean.setInterceptorNames("foo");
//        proxyFactoryBean.setTargetName("foo");
        Advisor[] advisors = bean.getAdvisors();
        for (Advisor advisor : advisors) {
            System.out.println(advisor.getAdvice().toString());
        }
        return bean;
    }
}
