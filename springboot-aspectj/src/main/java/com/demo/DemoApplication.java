package com.demo;

import com.demo.service.HelloWorldService;
import com.demo.common.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true) //proxyTargetClass = true强制使用cglib
public class DemoApplication {
    public static void main(String[] args) {
        final ConfigurableApplicationContext run = SpringApplication.run(DemoApplication.class, args);
        final HelloWorldService bean = run.getBean(HelloWorldService.class);
       /* String result = bean.hello("john");
        System.out.println("result : "+result);*/


        bean.helloTo(new Person("zhangsan"));
//        bean.hello("lisi");
    }



}
