package com.demo.service.impl;

import com.demo.aspectj.LogPrintAnnotation;
import com.demo.common.Person;
import org.springframework.stereotype.Service;

@Service
public class HelloWorldServiceImpl implements com.demo.service.HelloWorldService {

    @Override
    @LogPrintAnnotation
    public String hello(String name){
        String result = "hello "+name;
        System.out.println(result);
        return result;
    }

    @Override
    public String helloTo(Person person){
        String result = "hello "+person.getName();
        System.out.println(result);
        return result;
    }
}
