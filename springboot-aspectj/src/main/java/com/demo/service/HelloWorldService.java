package com.demo.service;

import com.demo.aspectj.LogPrintAnnotation;
import com.demo.common.Person;

public interface HelloWorldService {
    @LogPrintAnnotation
    String hello(String name);

    String helloTo(Person person);
}
