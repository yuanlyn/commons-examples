package com.demo.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AnnotationPointCutAspectJ {
    @Pointcut("@annotation(LogPrintAnnotation)")
    public void annoAspect(){}

    @Before("annoAspect()")
    public void before(JoinPoint joinPoint){
        System.out.println(" annoAspect before");
    }
}
