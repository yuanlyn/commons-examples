package com.demo.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TargetAspect {
    @Pointcut("target(com.demo.service.impl.HelloWorldServiceImpl)")
    public void targetAspect(){}

    @Before("targetAspect()")
    public void before(JoinPoint joinPoint){
        System.out.println("target aspect before");
    }
}
