package com.demo.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ArgsPointCutAspect {
    //@Pointcut("!within(org.springframework..*) && !within(com.sun..*) && @args(com.demo.aspectj.common.ArgsAnnotation)")
    //@Pointcut("within(com.demo.aspectj..*) && @args(com.demo.aspectj.common.ArgsAnnotation)")
    @Pointcut("within(com.demo.aspectj..*) && args(com.demo.common.Person)")
    public void argsPointCut(){}


    @Before("argsPointCut()")
    public void before(JoinPoint joinPoint){
        System.out.println("argsPointCut before:"+joinPoint.getArgs()[0].toString());
    }
}
