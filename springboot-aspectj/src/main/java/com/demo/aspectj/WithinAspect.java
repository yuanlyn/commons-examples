package com.demo.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class WithinAspect {

    @Pointcut("within(com.demo.service.impl.HelloWorldServiceImpl))")
    public void withinaspect(){}

    @Before("withinaspect()")
    public void before(JoinPoint joinPoint){
        System.out.println("with in aspect before ");
    }

}
