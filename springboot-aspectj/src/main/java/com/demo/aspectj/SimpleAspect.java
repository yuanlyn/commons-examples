package com.demo.aspectj;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SimpleAspect {

    @Pointcut("execution(* com.demo.service.*.*(..))")
    public void simpelPointCut(){
    }

    @Before("simpelPointCut()")
    public void before(JoinPoint joinPoint){
        System.out.println("execution before:");
        /*System.out.println("args:");
        for (int i = 0; i < joinPoint.getArgs().length; i++) {
            final Object arg = joinPoint.getArgs()[i];
            System.out.println("args"+i+":"+arg);
        }*/
    }

//    @After("simpelPointCut()")
    public void after(JoinPoint joinPoint){
        System.out.println("execution after:");
//        System.out.println( joinPoint.getKind());

    }

//    @Around("simpelPointCut()")
    public Object arount(ProceedingJoinPoint joinPoint){
        Object proceed = null;
        try {
            System.out.println("execution arount before");
            proceed = joinPoint.proceed(joinPoint.getArgs());
            System.out.println("execution around after :"+proceed.toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return proceed;
    }

//    @AfterReturning("simpelPointCut()")
    public void afterreturn(JoinPoint joinPoint){
        System.out.println("execution after return ...");
    }


}
