package com.common.generic;

/**
 * Created by yl on 2019/12/16
 *
 * PECS模型
 */
public class generic {

    public static class A extends B{}
    public static class B extends C{}
    public static class C{}

    public static class Example<T>{}

    public static void main(String[] args) {
        Example<? extends A> example1 = new Example<A>();
        Example<? extends B> example2 = new Example<A>();
        Example<? extends C> example3 = new Example<>();


        Example<? super A> example4 = new Example<C>();
        Example<? super B> example5 = new Example<C>();
        Example<? super C> example6 = new Example<>();


        ///////////////////////////////

        Example<? extends D> example7 = new Example<E>();
        Example<? extends D> example8 = new Example<F>();
        Example<? extends D> example9 = new Example<>();

        Example<? super D> example10 = new Example<>();
    }

    static interface D{};
    static interface E extends D{};
    static interface F extends E{};


}
