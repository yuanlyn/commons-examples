package com.common.fori;

/**
 * Created by yl on 2019/12/13
 */
public class For {
    public static void main(String[] args) {
        for (int i = 0; i < 3; ++i) {
            System.out.print(i);
            System.out.print(",");
        }
        System.out.printf("\n");
        for (int i = 0; i < 3; i++) {
            System.out.print(i);
            System.out.print(",");
        }

        ///for执行顺序：for[0],for[1],{code},for[2],for[1],{code},for[2],for[1],{code},for[2],...
        //for[2]在{code}之后执行，所以for[2]使用i++和++i是一样的
    }
}
