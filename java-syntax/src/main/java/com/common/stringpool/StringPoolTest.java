package com.common.stringpool;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class StringPoolTest {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        String string1 = "abcdefg";
        String string2 = "abcdefg";

        System.out.println(string1);
        System.out.println(string2);
//        Field f = Unsafe.class.getDeclaredField("theUnsafe");
//        f.setAccessible(true);
//        Unsafe unsafe = (Unsafe) f.get(null);
        System.out.println(string1 == string2);
//        abcdefg
//        abcdefg
//        true

//        String string1 = new String( "abcdefg");
//        String string2 = new String( "abcdefg");
//
//        System.out.println(string1);
//        System.out.println(string2);
//        System.out.println(string1 == string2);
//        abcdefg
//        abcdefg
//        false

//        String string1 = new String( "abcdefg");
//        string1.intern();
//        String string2 = new String( "abcdefg");
//        System.out.println(string1);
//        System.out.println(string2);
//        System.out.println(string1 == string2);
    }
}
