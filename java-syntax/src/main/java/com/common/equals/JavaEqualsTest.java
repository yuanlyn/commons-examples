package com.common.equals;

import java.util.Objects;

public class JavaEqualsTest {

    public static class Person{
        String name;
        public Person(String name){
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return name.equals(person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }
    }

    public static void main(String[] args) {
        Person person1 = new Person("zhangsan");
        Person person2 = new Person("zhangsan");
        System.out.println(person1 == person2);
    }
}
