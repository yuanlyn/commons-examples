package j8_lamda;

import org.junit.Test;

import java.util.stream.Stream;

/**
 * Created by yl on 2019/10/8
 */
public class LamdaTest_1 {
    @Test
    public void test1(){
        Stream.iterate(0, n -> n + 2).forEach(System.out::println);
    }
}
