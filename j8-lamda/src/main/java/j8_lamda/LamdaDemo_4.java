package j8_lamda;

import java.util.function.Function;

/**
 * Created by yl on 2019/10/8
 * Function不光会消费当前对象，还会根据当前对象生成新的对象
 */
public class LamdaDemo_4 {

    class printer{
        String print(Function<String,String> f){
            String s = "hello world";
           return f.apply(s);
        }

    }

    public static void main(String[] args) {
        printer printer = new LamdaDemo_4().new printer();

        String result = printer.print((s) -> {
            return s.concat(",").concat(s);
        });
        System.out.println(result);
    }
    

}
