package j8_lamda;

import java.util.function.Supplier;

/**
 * Created by yl on 2019/10/8
 * 静态方法里面无法将一个类的成员方法当作入参，必须通过该类的实例
 * 感觉java8中方法仍然是不能脱离类实例而单独运行的，这一点跟python把函数当作第一等公民不同，python中可以任意定义
 * 一个函数并当作参数使用，不用知道函数属于谁
 */
public class LamdaDemo_2 {

    class printer{
        void print(Supplier<String> supplier){
            System.out.println(supplier.get());
        }
    }

    public static void main(String[] args) {
        printer printer = new LamdaDemo_2().new printer();

        printer.print(new LamdaDemo_2()::getWorld);

        printer.print(() -> {return "hello";});
    }
    public String getWorld(){
        return "hello world!";
    }

}
