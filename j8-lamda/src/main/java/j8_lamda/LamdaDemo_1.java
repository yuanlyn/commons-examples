package j8_lamda;

import org.junit.Test;

/**
 * Created by yl on 2019/10/8
 * lamda表达式中this指向的是调用该表达式的对象，该例子中就是调用scopeExperiment的对象，该对象只能是LamdaDemo_1类的实例
 * 而匿名内部类中方法的中的this仍然指向该内部类，因为仍然是由内部类调用的该方法
 */
public class LamdaDemo_1 {
    private String value = "Enclosing scope value";


    public String scopeExperiment() {
        //该变量为局部变量
        String value = "Lambda value";
        Foo fooIC = new Foo() {
            String value = "Inner class value";

            @Override
            public String method(String string) {
                return this.value;
            }
        };
        String resultIC = fooIC.method("");

        Foo fooLambda = parameter -> {
            //该变量为局部变量
            //String value = "Lambda value";
            return this.value;
        };
        String resultLambda = fooLambda.method("");

        return "Results: resultIC = " + resultIC +
                ", resultLambda = " + resultLambda;
    }

    @FunctionalInterface
    private interface Foo {
        String method(String string);
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        LamdaDemo_1 demo_1 = LamdaDemo_1.class.newInstance();
        System.out.println(demo_1.scopeExperiment());
    }
}
