package j8_lamda;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Created by yl on 2019/10/8
 * range生成一个数组
 * peek 用来消费元素，不会返回新元素，所以不会改变原来的流
 */
public class LambdaDemo {
    public static void main(String[] args) {
        long count = IntStream.range(1,10).peek(LambdaDemo::debug).count();
        System.out.println("count=" + count);
    }

    public static void debug(int i) {
        System.out.println(Thread.currentThread().getName() + " " + " debug " + i);
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
