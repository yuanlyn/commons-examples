package j8_lamda;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortedLamda {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        list.add(new Person(1,"zhangsan","a"));
        list.add(new Person(1,"zhangsan2","b"));
        list.add(new Person(1,"zhangsan3","b"));

        list.stream().sorted(Comparator.comparing(Person::getId));
    }
}
