package j8_lamda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by yl on 2019/10/8
 * peek方法因为入参是Consumer，所以不会改变原来流的内容
 */
public class LamdaDemo_5 {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,2,3,3,4,5);

        list.stream().filter(x -> x>3).peek(x -> {
            System.out.println(x);
        }).collect(Collectors.toList()).forEach(x ->{
            System.out.println(x);
        });
    }
    

}
