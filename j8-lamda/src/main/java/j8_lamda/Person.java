package j8_lamda;

import lombok.*;

/**
 * Created by yl on 2019/10/8
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private int id;
    private String name;
    private String address;
}
