package j8_lamda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by yl on 2019/10/8
 * Java8 List转换Map
 */

public class DemoList2Map {
    private static List<Person> personList = new ArrayList<>();
    static{
        personList.add(Person.builder().id(20).name("zhangsan").address("shanghai").build());
        personList.add(Person.builder().id(30).name("lisi").address("nanjing").build());
    }
    public static void main(String[] args) {
        //Java8 List转换Map
        Map<Integer,Person> map_ = personList.stream().collect(Collectors.toMap((key->key.getId()),(value->value)));
        map_.forEach((key,value)-> System.out.println(key + ":" + value));
        Map<Integer, Person> mappedMovies = personList.stream().collect(
                Collectors.toMap(Person::getId,(value->value)));

        mappedMovies.forEach((key,value)->{
            System.out.println(key+":"+value);
        });
    }

}
