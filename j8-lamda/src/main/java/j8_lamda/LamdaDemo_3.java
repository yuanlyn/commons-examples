package j8_lamda;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by yl on 2019/10/8
 * Consumer消费者，直接消费，不会对原来的流进行加工从而产生新的对象
 */
public class LamdaDemo_3 {

    class printer{
        void print(Consumer<String> supplier){
            String s = "hello world";
            supplier.accept(s);
        }

    }

    public static void main(String[] args) {
        printer printer = new LamdaDemo_3().new printer();

        printer.print((s) -> {
            System.out.println(s);
        });
    }


}
