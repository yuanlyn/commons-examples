package com.example.demoweb1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/model")
public class ModelAttributeController {

    @ModelAttribute
    public void init(HttpServletRequest request){
        request.setAttribute("attr","attrValue");
    }

    @ModelAttribute
    public void init2(HttpServletRequest request){
        request.setAttribute("attr2","attrValue2");
    }

    @RequestMapping("/process")
    @ResponseBody
    public String process(@RequestParam String name,
                          @RequestAttribute String attr,
                          @RequestAttribute String attr2,
                          HttpServletRequest request,
                          HttpServletResponse response
    ) throws IOException {
//        request.setAttribute("attr",attr+attr);
//        request.setAttribute("attr2",attr2+attr2);//对于重定向来说不起作用哦
        request.getSession().setAttribute("token","token");

        response.sendRedirect("/model/process2?name="+name);
        return name+"."+attr+"."+attr2;
    }


    @RequestMapping("/process2")
    @ResponseBody
    public String process2(@RequestParam String name,
                          @RequestAttribute String attr,
                          @RequestAttribute String attr2,
                           @SessionAttribute String token
    ){

        return name+"."+attr+"."+attr2+"    token:"+token;
    }
}
