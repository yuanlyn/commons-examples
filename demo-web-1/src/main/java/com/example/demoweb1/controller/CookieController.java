package com.example.demoweb1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/cookie")
public class CookieController {

    @RequestMapping("/addCookie")
    public void addCookie(HttpServletRequest request, HttpServletResponse response){
        Cookie cookie = new Cookie("selfCookie", "selfCookieValue");
        cookie.setMaxAge(10);
        //cookie.setDomain("/localhost:18081");//不要以.或者/开头，dot是非法的，而/默认会被添加上
                                    //测试Apache Tomcat/9.0.27
        cookie.setComment("somecomment");
        cookie.setVersion(1);
        response.addCookie(cookie);
        response.addHeader("Set-Cookie","cookie2=admin;");
        try {
            response.getWriter().write("done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
