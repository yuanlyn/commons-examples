package com.example.demoweb1.controller;

import com.example.demoweb1.bean.ParamBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

@Controller
@RequestMapping("/convert")
public class PropertieConvertController extends BaseController{

    @RequestMapping("/toDate")
    @ResponseBody
    public String convertToDate(@RequestParam("date") Date date){
        System.out.println(date);
        return date.toString();
    }

    @RequestMapping("/toDateBean")
    @ResponseBody
    public ParamBean convertToDate(@RequestBody ParamBean date){
        System.out.println(date);
        return date;
    }
}
