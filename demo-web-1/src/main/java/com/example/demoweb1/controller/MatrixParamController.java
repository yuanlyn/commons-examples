package com.example.demoweb1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 不建议使用过于复杂的传参方式
 */

@Controller
@RequestMapping("/matrix")
public class MatrixParamController {

    @RequestMapping("/param/{ids}/param/{ids}")
    public void matrix(@MatrixVariable(name = "id",pathVar = "id")String id,@MatrixVariable(name = "list",pathVar = "list") List<String> list)
    {

        System.out.println(id);
        System.out.println(list);
    }
}
