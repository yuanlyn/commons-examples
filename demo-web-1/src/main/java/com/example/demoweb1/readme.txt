1.重定向如何设置cookie，设置到response里

2.  @CookieValue注解用来指定一个参数是从cookie中获取的

3.    @InitBinder 用来为controller绑定一些类型转换的工作，比如，如果接收的参数是Date类型，而入参实际上只能是字符串类型，可以通过
    @InitBinder将一个解释器绑定到Date入参上
        @InitBinder只能用在Controller层，所以我们开发一个BaseController当作基类

 4. 如果是通过RequestBody传值，需要转化为javabean，但是如果包含如Date类型的属性，就需要Json2Message转化器来支持
 可以通过注入MappingJackson2HttpMessageConverter转化器或者是GsonHttpMessageConverter来实现，如果注入多个，只有后
 一个会生效

5.@MatrixVariable注解使参数更复杂

6.@ModelAttribute，用来将一个值绑定到model中，可用在参数或者方法上
因为开发ajax请求比较多，不返回ModelAndView，所以该注解较少使用

7.@RequestAttribute，用来绑定request中的attribute到方法的入参，这些属性一般来自Filter设置，
或者来自@ModelAttribute修饰的方法绑定的，或者转发请求时加入的属性，本身不会是来自用户的输入

8.@SessionAttribute,绑定session属性值到指定方法入参

9.通过注入自定义的MappingJackson2HttpMessageConverter，来改变某些类型的反序列化形式

10.通过设置WebMvcConfigurer，
public void configurePathMatch(PathMatchConfigurer configurer) {
    UrlPathHelper urlPathHelper=new UrlPathHelper();
    urlPathHelper.setRemoveSemicolonContent(true);
    configurer.setUrlPathHelper(urlPathHelper);
}
将提出url里面的分号，如访问：
http://127.0.0.1:18081/convert/toDateBean
http://127.0.0.1:18081/convert/toDateBean;
http://127.0.0.1:18081/convert;/toDateBean;
将是一样的效果，注意分号只能在url地址的每一段的末尾，不能放在前面，如：
http://127.0.0.1:18081/convert/;toDateBean




