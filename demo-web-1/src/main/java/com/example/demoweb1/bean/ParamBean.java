package com.example.demoweb1.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ParamBean {

    private Date date;
    private List<String> strs;
    private Map<String,Object> maps;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    public List<String> getStrs() {
        return strs;
    }

    public void setStrs(List<String> strs) {
        this.strs = strs;
    }

    public Map<String, Object> getMaps() {
        return maps;
    }

    public void setMaps(Map<String, Object> maps) {
        this.maps = maps;
    }


    @Override
    public String toString() {
        return "ParamBean{" +
                "date=" + date +
                ", strs=" + strs +
                ", maps=" + maps +
                '}';
    }
}
