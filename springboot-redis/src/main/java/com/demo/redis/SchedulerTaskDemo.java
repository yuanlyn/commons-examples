package com.demo.redis;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
@EnableScheduling
public class SchedulerTaskDemo implements InitializingBean {
    @Autowired
    private RedisTemplate redisTemplate;
    private AtomicInteger inc = new AtomicInteger(1);

    /*
    如果哨兵少于2个，则会报错，且无法获取数据，当哨兵存活大于等于2个但是不全存活，会报错，但仍能获取数据
    当master挂掉后，会在slave中重新生成master进行写，读的话，3个节点都可以，但是写只能在master上
     */
//    @Scheduled(cron = "*/2 * * * * *")
//    public void run(){
//        redisTemplate.setValueSerializer(new GenericToStringSerializer<Long>(Long.class));
//        try{
//            redisTemplate.opsForValue().set("stringValueKey",inc.getAndIncrement());
//            Long value = (Long) redisTemplate.opsForValue().getAndSet("stringValueKey",inc.getAndIncrement());
//            System.out.println(value);
//        }catch (Exception e){
//
//            e.printStackTrace();
//        }
//    }


    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
