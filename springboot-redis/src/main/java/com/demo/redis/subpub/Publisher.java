package com.demo.redis.subpub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Component
public class Publisher {
    @Autowired
    private RedisTemplate redisTemplate;

    public void pub(String topic,String message){
        redisTemplate.getConnectionFactory().getConnection().publish(topic.getBytes(),message.getBytes());
        System.out.println("发布了消息");
    }

    public void pub(String topic,byte[] message){
        redisTemplate.getConnectionFactory().getConnection().publish(topic.getBytes(),message);
        System.out.println("发布了消息");
    }
}
