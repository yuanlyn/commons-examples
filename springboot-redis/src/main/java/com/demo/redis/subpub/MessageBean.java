package com.demo.redis.subpub;

import java.io.Serializable;

public class MessageBean  implements Serializable {

    private String message;

    public MessageBean() {
    }

    public MessageBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageBean{" +
                "message='" + message + '\'' +
                '}';
    }
}
