package com.demo.redis.subpub;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

@Component
public class Consumer implements MessageListener, InitializingBean {

    @Autowired
    StringRedisSerializer stringRedisSerializer;
    @Override
    public void onMessage(Message message, byte[] pattern) {

        System.out.println(String.format("收到来自%s的消息：%s", stringRedisSerializer.deserialize(pattern),message.toString()));
    }
    @Autowired
    RedisMessageListenerContainer container;
    @Override
    public void afterPropertiesSet() throws Exception {
        container.addMessageListener(this, ChannelTopic.of("chat"));
    }
}
