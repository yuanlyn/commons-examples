package com.demo.redis.bloomfilter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "f4")
public class HashCodeDirect4 implements HashCode {
    @Override
    public int hashcode(Object object) {
        return object.hashCode() << 12 & object.hashCode() >> 20;
    }
}
