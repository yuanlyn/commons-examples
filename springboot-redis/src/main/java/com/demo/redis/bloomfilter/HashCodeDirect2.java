package com.demo.redis.bloomfilter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "f2")
public class HashCodeDirect2 implements HashCode {
    @Override
    public int hashcode(Object object) {
        return object.hashCode() << 4 & object.hashCode() >> 28;
    }
}
