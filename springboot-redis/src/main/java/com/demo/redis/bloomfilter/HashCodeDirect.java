package com.demo.redis.bloomfilter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "f1")
public class HashCodeDirect implements HashCode {
    @Override
    public int hashcode(Object object) {
        return object.hashCode();
    }
}
