package com.demo.redis.bloomfilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BloomFilter {
    @Autowired
    RedisTemplate redisTemplate;

    List<HashCode> hashCode = new ArrayList<HashCode>(){
        {
            add(new HashCodeDirect());
            add(new HashCodeDirect2());
            add(new HashCodeDirect3());
            add(new HashCodeDirect4());
        }
    };
    /**
     *
     * @param object
     * @return  true 表示还没有此object，false表示已经包含此object
     */
    public boolean add(Object object){
        boolean result = getConn().setBit(topic,hashCode.get(0).hashcode(object),true);
        boolean result2 = getConn().setBit(topic,hashCode.get(1).hashcode(object),true);
        boolean result3 = getConn().setBit(topic,hashCode.get(2).hashcode(object),true);
        boolean result4 = getConn().setBit(topic,hashCode.get(3).hashcode(object),true);
//        int hash = hashCode.hashcode(object);
//        boolean result = getConn().setBit(topic,hash,true);
        return result && result2 && result3 && result4;
    }

    public RedisConnection getConn(){
        return redisTemplate.getConnectionFactory().getConnection();
    }
    private byte[] topic = "bloomKey".getBytes();

}
