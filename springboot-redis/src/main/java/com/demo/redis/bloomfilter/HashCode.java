package com.demo.redis.bloomfilter;

public interface HashCode {
    public int hashcode(Object object);
}
