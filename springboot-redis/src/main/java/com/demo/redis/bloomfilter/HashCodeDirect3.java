package com.demo.redis.bloomfilter;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier(value = "f3")
public class HashCodeDirect3 implements HashCode {
    @Override
    public int hashcode(Object object) {
        return object.hashCode() << 8 & object.hashCode() >> 24;
    }
}
