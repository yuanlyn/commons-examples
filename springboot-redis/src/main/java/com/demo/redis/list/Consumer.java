package com.demo.redis.list;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component(value = "PopConsumer")
@ConditionalOnProperty(value = "testList",havingValue = "true")
public class Consumer implements InitializingBean {
    @Autowired
    RedisTemplate redisTemplate;
    @Override
    public void afterPropertiesSet() throws Exception {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    Map map = (Map) redisTemplate.opsForList().rightPop("redis-list");
                    if(map != null){
                        System.out.println(map);
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
}
