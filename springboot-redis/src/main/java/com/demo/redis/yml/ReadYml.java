package com.demo.redis.yml;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;

@Component
public class ReadYml implements InitializingBean {

    private void readYml(){
        InputStream inputStream = ReadYml.class.getResourceAsStream("demo.yml");
        if(inputStream == null){
            inputStream = ReadYml.class.getClassLoader().getResourceAsStream("demo.yml");
        }
        Yaml yaml = new Yaml();
        Map<String,String> originMap = yaml.load(inputStream);

        System.out.println(originMap);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        readYml();
    }
}
