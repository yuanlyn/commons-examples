package com.demo.redis.hash;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class HashMapTest {
    @Autowired
    private RedisTemplate redisTemplate;

    public void test(){
        redisTemplate.opsForHash().put("object-key","username","root");
        redisTemplate.opsForHash().delete("object-key");
//        redisTemplate.opsForValue().set();
    }
}
