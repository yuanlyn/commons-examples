package com.demo.redis;

import com.demo.redis.bloomfilter.BloomFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = {DemoRedisApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class BloomFilterMain {

    @Autowired
    BloomFilter bloomFilter;

    @Test
    public void test(){
        System.out.println(bloomFilter.add("1"));
    }
}
