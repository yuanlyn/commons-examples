package com.demo.crawler;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpUtils {
    public static void main(String[] args) {
        getHtml();
    }
    public static void getHtml(){
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        // 创建Get请求
        HttpGet httpGet = new HttpGet("https://kaiwu.lagou.com/course/courseInfo.htm?courseId=31#/detail/pc?id=1025");
        // 响应模型
        CloseableHttpResponse response = null;
        try {
            httpGet.setHeader("JSESSIONID","ABAAABAAAECABEHCCF91738AE5995273007BA03086FC691");
            httpGet.setHeader("JSESSIONID","ABAAAECABADAACHB9F7F46E10ECEB69600FDEB1EC79E804");
            httpGet.setHeader("LG_HAS_LOGIN","1");
            httpGet.setHeader("LG_LOGIN_USER_ID","f8d7e74d3974e80dac3d47ac15f8690f4ba21b3bf5a79f10126af4865ce079b8");
            httpGet.setHeader("RK","CBAQSbH4RO");
            httpGet.setHeader("_putrc","5DF44777FB454096123F89F2B170EADC");
            httpGet.setHeader("gate_login_token","a0266ca5891792486c21f96ae5c6ce306ffaa2341e70b805b10d769b8970bce7");
            httpGet.setHeader("login","true");
            httpGet.setHeader("pac_uid","0_6ef0b091c59f8");
            httpGet.setHeader("pgv_pvi","6451050496");
            httpGet.setHeader("pgv_pvid","4671271119");
            httpGet.setHeader("ptcz","4f9a27137180be161c05ccf81b6cfa0573ee5526f41ade94f79f62aaf8e227a1");
            httpGet.setHeader("sensorsdata2015jssdkcross","%7B%22distinct_id%22%3A%2218894501%22%2C%22first_id%22%3A%221747ddb348048c-043f6fcbed34ab-f7b1332-921600-1747ddb34814a2%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24os%22%3A%22Windows%22%2C%22%24browser%22%3A%22Chrome%22%2C%22%24browser_version%22%3A%2285.0.4183.83%22%7D%2C%22%24device_id%22%3A%221747ddb348048c-043f6fcbed34ab-f7b1332-921600-1747ddb34814a2%22%7D");
            httpGet.setHeader("sensorsdata2015session","%7B%7D");
            httpGet.setHeader("ticketGrantingTicketId","_CAS_TGT_TGT-100d7485dee04099adf45624c687a272-20200911235005-_CAS_TGT_");
            httpGet.setHeader("tvfe_boss_uuid","d0f85d31706df1e9");
            httpGet.setHeader("unick","%E7%94%A8%E6%88%B70895");
            // 由客户端执行(发送)Get请求
            response = httpClient.execute(httpGet);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();
            System.out.println("响应状态为:" + response.getStatusLine());
            if (responseEntity != null) {
                System.out.println("响应内容长度为:" + responseEntity.getContentLength());
                System.out.println("响应内容为:" + EntityUtils.toString(responseEntity));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
