import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class TestUrlParse {
    public static void main(String[] args) {
        try {
            List<NameValuePair> pairs = URLEncodedUtils.parse(new URI("http://www.baidu.com?aaa=123&&bbb=456"), "UTF-8");
            for (int i = 0; i < pairs.size(); i++) {
                NameValuePair nameValuePair = pairs.get(i);
                System.out.println(nameValuePair.getName() + " ==> " + nameValuePair.getValue());
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
