package org.mitre.dsmiley.httpproxy;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;

public interface RequestBodyEnhanceFilter {
    public boolean care(HttpServletRequest request);
    public InputStream getExchangeInput(HttpServletRequest request);
}
