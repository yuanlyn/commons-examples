package org.mitre.dsmiley.httpproxy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ProxyFilter {

    public boolean matched(HttpServletRequest request);

    public void filter(HttpServletRequest request, HttpServletResponse response);
}
