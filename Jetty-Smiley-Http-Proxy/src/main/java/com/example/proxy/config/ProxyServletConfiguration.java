package com.example.proxy.config;

import org.mitre.dsmiley.httpproxy.ParameterTemplateProxyServlet;
import org.mitre.dsmiley.httpproxy.ProxyFilter;
import org.mitre.dsmiley.httpproxy.ProxyServlet;
import org.mitre.dsmiley.httpproxy.URITemplateProxyServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class ProxyServletConfiguration implements EnvironmentAware {

    @Bean
    public ProxyFilter proxyFilter(){
        return new ProxyFilter() {
            @Override
            public boolean matched(HttpServletRequest request) {
                return true;
            }

            @Override
            public void filter(HttpServletRequest request, HttpServletResponse response) {
            }
        };
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean(){
        ParameterTemplateProxyServlet proxyServlet = new ParameterTemplateProxyServlet();

        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(proxyServlet,
                "/module-01/*");
        servletRegistrationBean.addInitParameter(ProxyServlet.P_TARGET_URI,"http://192.168.43.131:19003/module-01");
        servletRegistrationBean.addInitParameter(ProxyServlet.P_LOG, "false");
        servletRegistrationBean.addInitParameter(ProxyServlet.P_PRESERVECOOKIES, "true");
        return servletRegistrationBean;
    }

    /*@Bean
    public ServletRegistrationBean urlTemplateServletProxy(){
        URITemplateProxyServlet proxyServlet = new URITemplateProxyServlet();
        //proxyServlet.getServletConfig().getInitParameterNames();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(proxyServlet,
                "/module-01/login.action");
        servletRegistrationBean.addInitParameter(ProxyServlet.P_TARGET_URI,"http://192.168.43.131:19003/module-01/login.action?username=root&password=123456");
        servletRegistrationBean.addInitParameter(ProxyServlet.P_LOG, "false");
        return servletRegistrationBean;
    }*/

    private RelaxedPropertyResolver propertyResolver;

    @Override
    public void setEnvironment(Environment environment) {
        this.propertyResolver = new RelaxedPropertyResolver();
    }


}
