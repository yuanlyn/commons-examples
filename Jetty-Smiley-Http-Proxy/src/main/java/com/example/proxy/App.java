package com.example.proxy;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.mitre.dsmiley.httpproxy.ProxyServlet;

import javax.servlet.http.HttpServlet;

public class App {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/proxy");

        HttpServlet proxyServlet = new ProxyServlet();
        ServletHolder servletHolder = new ServletHolder();
        servletHolder.setServlet(proxyServlet);
        servletHolder.setInitParameter("targetUri", "https://www.baidu.com");
        servletContextHandler.addServlet(servletHolder, "/baidu/*");

        server.start();
    }
}