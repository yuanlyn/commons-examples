package hibernate.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

/**
 * Created by yl on 2019/10/17
 */
@Entity
@Table(name = "TEST_AAAAA", schema = "BDAP", catalog = "default")
public class TestAaaaa {
    private long id;
    private Time time;
    private String name;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TIME", nullable = true)
    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    @Basic
    @Column(name = "NAME", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestAaaaa testAaaaa = (TestAaaaa) o;
        return id == testAaaaa.id &&
                Objects.equals(time, testAaaaa.time) &&
                Objects.equals(name, testAaaaa.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, name);
    }
}
