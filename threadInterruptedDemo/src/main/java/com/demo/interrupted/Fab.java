package com.demo.interrupted;

public class Fab {
    public static int fab(int a){
        if(a < 0)return 0;
        if(a == 0)return a;
        if(a == 1)return a;
        return fab(a - 1) + fab(a - 2);
    }
}
