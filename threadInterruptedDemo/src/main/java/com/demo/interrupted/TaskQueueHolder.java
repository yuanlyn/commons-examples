package com.demo.interrupted;

import java.util.concurrent.LinkedBlockingQueue;

public class TaskQueueHolder {
    private static class Holder{
        private static LinkedBlockingQueue<String> TaskQueue = new LinkedBlockingQueue<>();

    }
    public static  LinkedBlockingQueue<String> get(){
        return Holder.TaskQueue;
    }
}
