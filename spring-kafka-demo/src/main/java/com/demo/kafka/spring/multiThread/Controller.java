package com.demo.kafka.spring.multiThread;

import com.demo.kafka.spring.multiThread.common.Bar1;
import com.demo.kafka.spring.subpub.common.Foo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	private KafkaTemplate<Object, Object> template;

	@RequestMapping(path = "/send/foo/{what}")
	public void sendFoo(@PathVariable String what) {
		Foo1 foo1 = new Foo1(what);
		foo1.setName("nihao");
		this.template.send("foos", foo1);
	}


	@RequestMapping(path = "/send/bar/{what}")
	public void sendBar(@PathVariable String what) {
		this.template.send("bars", new Bar1(what));
	}
}
