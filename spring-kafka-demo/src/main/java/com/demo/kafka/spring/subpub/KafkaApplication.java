package com.demo.kafka.spring.subpub;

import com.demo.kafka.spring.subpub.common.Foo1;
import com.demo.kafka.spring.subpub.common.Foo2;
import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;

@SpringBootApplication
public class KafkaApplication {
    private final Logger logger = LoggerFactory.getLogger(KafkaApplication.class);
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext
                = SpringApplication.run(KafkaApplication.class,args);
    }


    @Bean
    public ConcurrentKafkaListenerContainerFactory getKafkaListenerContainerFactory(
            ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
            ConsumerFactory<Object, Object> kafkaConsumerFactory,
            KafkaTemplate<Object, Object> template
    ){
        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory();
        configurer.configure(factory,kafkaConsumerFactory);
        // 配置一个消费的消息处理异常处理器：重试3次还不成功则将该消息发到死信topic里。
        factory.setErrorHandler(
                new SeekToCurrentErrorHandler(
                        new DeadLetterPublishingRecoverer(template), 3)); // dead-letter

        return factory;
    }

    @Bean
    public RecordMessageConverter converter() {
        return new StringJsonMessageConverter();
    }

    @KafkaListener(id = "fooGroup", topics = "topic1")
    public void listen(Foo1 foo) {
        logger.info("1  Received: " + foo);
        if (foo.getFoo().startsWith("fail")) {
            throw new RuntimeException("failed");
        }
    }
    @KafkaListener(id = "fooGroup", topics = "topic1")
    public void listen2(Foo1 foo) {
        logger.info("2  Received: " + foo);
        if (foo.getFoo().startsWith("fail")) {
            throw new RuntimeException("failed");
        }
    }

    @KafkaListener(id = "dltGroup", topics = "topic1.DLT")
    public void dltListen(String in) {
        logger.info("Received from DLT: " + in);
    }

    @Bean
    public NewTopic topic() {
        return new NewTopic("topic1", 1, (short) 1);
    }

    @Bean
    public NewTopic dlt() {
        return new NewTopic("topic1.DLT", 1, (short) 1);
    }

    @Bean
    public NewTopic automadetopic() {
        return new NewTopic("topic1.automadetopic", 1, (short) 1);
    }
}
