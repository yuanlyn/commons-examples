package com.demo.kafka.spring.multiThread;

import com.demo.kafka.StartProperties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;

public class FooConsumer {
    public static void main(String[] args) {

        try (KafkaConsumer<String, String> consumer =
                     new KafkaConsumer<>(StartProperties.getDefaultProperties("fooGroup"));) {
            // 订阅topics
            consumer.subscribe(Arrays.asList("foos"));
//            consumer.seek(new TopicPartition("test", 1), 0);
            while (true) {
                // kafka中是拉模式，poll的时间参数是告诉Kafka:如果当前没有数据，等待多久再响应
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1L));
                for (ConsumerRecord<String, String> record : records)
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());

                // 设定从哪里开始拉取消息 （需要时设置）
                // consumer.seek(new TopicPartition(topic, 0), 0);
            }
        }
    }
}
