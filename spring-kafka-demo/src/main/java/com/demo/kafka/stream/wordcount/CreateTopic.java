package com.demo.kafka.stream.wordcount;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;

public class CreateTopic {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.1.6:9091,192.168.1.6:9092,192.168.1.6:9093");

		try (AdminClient admin = AdminClient.create(props);) {
			admin.createTopics(Collections.singletonList(new NewTopic("source-topic", 1, (short) 1)));
			admin.createTopics(Collections.singletonList(new NewTopic("sink-topic", 1, (short) 1)));
			System.out.println("创建 topic 完成！");
		}

	}
}
