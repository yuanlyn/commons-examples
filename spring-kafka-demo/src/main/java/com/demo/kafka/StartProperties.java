package com.demo.kafka;

import org.apache.kafka.clients.CommonClientConfigs;

import java.util.Properties;

public class StartProperties {
    public static Properties getDefaultProperties(String consumergroup) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.1.6:9091,192.168.1.6:9092,192.168.1.6:9093");
        props.put(CommonClientConfigs.CLIENT_ID_CONFIG, "0");
        // 设置消费组
        props.put("group.id", consumergroup);
        // 开启自动消费offset提交
        // 如果此值设置为true，consumer会周期性的把当前消费的offset值保存到zookeeper。当consumer失败重启之后将会使用此值作为新开始消费的值。
        props.put("enable.auto.commit", "true");
        // 自动消费offset提交的间隔时间
        props.put("auto.commit.interval.ms", "1000");

        // key 的序列化器
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // key 的反序列化器
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        // 消息的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 消息的反序列化器
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        return props;
    }
}
