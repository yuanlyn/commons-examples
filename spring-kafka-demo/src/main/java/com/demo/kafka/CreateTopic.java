package com.demo.kafka;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;

public class CreateTopic {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.1.6:9091,192.168.1.6:9092,192.168.1.6:9093");

		try (AdminClient admin = AdminClient.create(props);) {
			admin.createTopics(Collections.singletonList(new NewTopic("test-partition-3", 3, (short) 3)));
		}

	}
	/*
	创建topic，3个分片，每个分片3个备份（包含本身共3个备份）
	{"controller_epoch":6,"leader":0,"version":1,"leader_epoch":0,"isr":[0,2,1]}
	{"controller_epoch":6,"leader":2,"version":1,"leader_epoch":0,"isr":[2,1,0]}
	{"controller_epoch":6,"leader":1,"version":1,"leader_epoch":0,"isr":[1,0,2]}
	 */
}
