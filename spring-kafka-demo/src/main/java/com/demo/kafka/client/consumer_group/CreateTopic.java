package com.demo.kafka.client.consumer_group;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;

public class CreateTopic {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "192.168.1.6:9091,192.168.1.6:9092,192.168.1.6:9093");

		try (AdminClient admin = AdminClient.create(props);) {
//			admin.deleteTopics(Collections.singletonList("test-group"));
			admin.createTopics(Collections.singletonList(new NewTopic("test-group", 2, (short) 1)));
			System.out.println("创建Topic ok！");
		}catch (Exception e){
			e.printStackTrace();
		}finally {

		}

	}
}
