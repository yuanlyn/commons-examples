package java_characters.inheritance_character;

public class Subclass extends SuperClass {


    public void  getSuperPrivateField(){
//        System.out.println("subclass get name from superclass : " + super.name);
        System.out.println("subclass get age from superclass : "+ super.age);
    }

    public void  execSuperPrivateMethod(){
        System.out.println("i am subclass, i want invoke the private method from superclass");
//        super.sayHi();


        super.saySecret();
    }

    public static void main(String[] args) {
        Subclass subclass = new Subclass();
        subclass.getSuperPrivateField();
        subclass.execSuperPrivateMethod();
    }


}
