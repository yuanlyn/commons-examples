package java_characters.inheritance_character;

public class SuperClass
{
    private String name = "susan";

    private void sayHi(){
        System.out.println("superclass say hi");
    }


    protected String age = "22";
    protected void saySecret(){
        System.out.println("superclass say a secret ");
    }
}
