package com.demo.filewatch;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by yl on 2019/11/26
 */
public class FileWatch {
    public static void main(String[] args) throws IOException {

        Map<WatchKey,Path> map = new HashMap<>();
        FileWatch fileWatch = new FileWatch();

        Path path1 = Paths.get("D:\\param\\b");
        WatchKey key1 = fileWatch.watch(path1);
        map.put(key1,path1);


        /*Path path2 = Paths.get("D:\\param\\b");
        WatchKey key2 = fileWatch.watch(path2);
        map.put(key2,path2);*/

//        System.out.println(key1 == key2);


        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for(;;){
                    try {
                        WatchKey key = fileWatch.getWatchService().take();
                        Path watch_dir = map.get(key);
                        List<WatchEvent<?>> events = key.pollEvents();
                        for (int i = 0; i < events.size(); i++) {
                            WatchEvent<Path> event = (WatchEvent<Path>) events.get(i);
                            Path path = event.context();
                            Path target_path = watch_dir.resolve(path);
                            System.out.println(target_path.toUri()+"::"+Thread.currentThread().getId());
                        }
                        key.reset();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();

//        Thread t2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for(;;){
//                    try {
//                        WatchKey key = fileWatch.getWatchService().take();
//                        Path watch_dir = map.get(key);
//                        List<WatchEvent<?>> events = key.pollEvents();
//                        for (int i = 0; i < events.size(); i++) {
//                            WatchEvent<Path> event = (WatchEvent<Path>) events.get(i);
//                            Path path = event.context();
//                            Path target_path = watch_dir.resolve(path);
//                            System.out.println(target_path.toUri()+"::"+Thread.currentThread().getId());
//                        }
//                        key.reset();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//        t2.start();
    }




    private WatchService watchService;
    public FileWatch() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();

    }
    public WatchKey watch(Path path) throws IOException {
        WatchKey key = path.register(watchService, ENTRY_CREATE);
        return key;
    }

    public WatchService getWatchService() {
        return watchService;
    }
}
