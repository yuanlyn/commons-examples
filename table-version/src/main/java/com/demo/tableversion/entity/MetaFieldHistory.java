package com.demo.tableversion.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 字段元数据
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MetaFieldHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    /**
     * table_id
     */
    private Integer tableId;

    @TableField("fieldName")
    private String fieldname;

    @TableField("fieldType")
    private String fieldtype;

    @TableField("fieldLength")
    private String fieldlength;

    private String comment;

    private String version;

    private String releaseStat;

    private String deleteFlag;


}
