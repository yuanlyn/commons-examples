package com.demo.tableversion.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class MetaTableVersionHistory {

    @TableField("table_id")
    private Integer tableId;
    @TableField("tableName")
    private String tableName;
    @TableField("version")
    private String version;
    @TableField("create_tm")
    private Date creatTm;
}
