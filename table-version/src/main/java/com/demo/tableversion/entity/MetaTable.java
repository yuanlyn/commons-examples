package com.demo.tableversion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 表元数据
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false, of = {"tablename","version","releaseStat","deleteFlag"})
public class MetaTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("tableName")
    private String tablename;

    private String version;

    private String releaseStat;

    private String deleteFlag;


}
