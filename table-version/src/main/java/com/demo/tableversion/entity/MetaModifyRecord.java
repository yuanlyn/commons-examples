package com.demo.tableversion.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 表/字段元数据修改历史
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MetaModifyRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * record_id
     */
    private Integer recordId;

    /**
     * modify_type,1:table,2:field
     */
    private Integer modifyType;

    /**
     * 修改类型（0：新增，1：修改，2：删除)
     */
    private Integer modifyOperationType;

    /**
     * table/field的id
     */
    private Integer modifyRecordId;

    /**
     * old value
     */
    private String oldValue;

    /**
     * new value
     */
    private String newValue;

    private String version;


}
