package com.demo.tableversion.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 表元数据
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MetaTableHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("id")
    private Integer id;

    @TableField("tableName")
    private String tablename;

    private String version;

    private String releaseStat;

    private String deleteFlag;


}
