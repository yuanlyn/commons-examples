package com.demo.tableversion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.tableversion.common.Version;
import com.demo.tableversion.controller.MetaTableVersion;
import com.demo.tableversion.controller.MetaVersionUtil;
import com.demo.tableversion.entity.MetaFieldHistory;
import com.demo.tableversion.entity.MetaTable;
import com.demo.tableversion.entity.MetaTableHistory;
import com.demo.tableversion.mapper.MetaTableHistoryMapper;
import com.demo.tableversion.mapper.MetaTableMapper;
import com.demo.tableversion.service.IMetaTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 表元数据 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Service
public class MetaTableServiceImpl implements IMetaTableService {
    @Autowired
    private MetaTableMapper metaTableMapper;
    @Autowired
    private MetaTableHistoryMapper metaTableHistoryMapper;

    @Override
    public int saveMetaTable(MetaTable metaTable){
        int insert = metaTableMapper.insert(metaTable);
        metaTable.setId(metaTableMapper.selectLastId());
        if(insert > 0){
            MetaTableHistory metaTableHistory = new MetaTableHistory();

            BeanUtils.copyProperties(metaTable,metaTableHistory);
            metaTableHistory.setId(metaTableMapper.selectLastId());
            metaTableHistoryMapper.insert(metaTableHistory);
        }
        /*if(1 == 1){
            throw new RuntimeException("this is test exception");
        }*/
        return insert;
    }

    @Override
    public MetaTable findOne(Integer id) {
        return metaTableMapper.selectById(id);
    }

    @Override
    public int modifyMetaTable(MetaTable metaTable) {
        if(metaTable.getId() == null){return 0;}

        int update = metaTableMapper.updateById(metaTable);
        if(update > 0){
            MetaTableHistory metaFieldHistory = new MetaTableHistory();
            BeanUtils.copyProperties(metaTable,metaFieldHistory);
            metaTableHistoryMapper.insert(metaFieldHistory);
        }
        return update;
    }

    @Override
    public void release(Integer id) {
        MetaTable metaTable = metaTableMapper.selectById(id);
        metaTable.setReleaseStat("1");
        metaTableMapper.updateById(metaTable);
    }

    @Override
    public void delete(Integer id) {
        MetaTable metaTable = metaTableMapper.selectById(id);
        metaTable.setDeleteFlag("1");
        metaTableMapper.updateById(metaTable);

    }

    @Override
    public void delete2(Integer id) {
        metaTableMapper.deleteById(id);

    }

    @Override
    public int restoreVersion(MetaTable metaTable) {
        return metaTableMapper.updateById(metaTable);
        // TODO
        //  if(metaTable.)
    }

    @Override
    public List<String> listVersion(Integer id){
        QueryWrapper<MetaTableHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("version").eq("id",id);
        List<MetaTableHistory> metaTableHistories = metaTableHistoryMapper.selectList(queryWrapper);
        return metaTableHistories.stream().map(MetaTableHistory::getVersion).collect(Collectors.toList());
    }

    @Override
    public List<MetaTableHistory> listMetaTableHistory(Integer id){
        QueryWrapper<MetaTableHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("version").eq("id",id);
        List<MetaTableHistory> metaTableHistories = metaTableHistoryMapper.selectList(queryWrapper);
        return metaTableHistories;
    }

    @Override
    public Integer selectLastId(){
        return metaTableMapper.selectLastId();
    }
}
