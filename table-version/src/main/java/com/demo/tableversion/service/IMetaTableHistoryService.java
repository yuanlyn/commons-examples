package com.demo.tableversion.service;

public interface IMetaTableHistoryService {
    int delete(Integer id);

    Integer getLastId();
}
