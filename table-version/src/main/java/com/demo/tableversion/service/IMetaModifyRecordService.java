package com.demo.tableversion.service;

import com.demo.tableversion.entity.MetaModifyRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 表/字段元数据修改历史 服务类
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface IMetaModifyRecordService extends IService<MetaModifyRecord> {

}
