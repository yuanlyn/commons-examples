package com.demo.tableversion.service.impl;

import com.demo.tableversion.entity.MetaModifyRecord;
import com.demo.tableversion.mapper.MetaModifyRecordMapper;
import com.demo.tableversion.service.IMetaModifyRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 表/字段元数据修改历史 服务实现类
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@Service
public class MetaModifyRecordServiceImpl extends ServiceImpl<MetaModifyRecordMapper, MetaModifyRecord> implements IMetaModifyRecordService {

}
