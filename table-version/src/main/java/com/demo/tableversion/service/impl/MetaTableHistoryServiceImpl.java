package com.demo.tableversion.service.impl;

import com.demo.tableversion.mapper.MetaTableHistoryMapper;
import com.demo.tableversion.service.IMetaTableHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MetaTableHistoryServiceImpl implements IMetaTableHistoryService {
    @Autowired
    private MetaTableHistoryMapper metaTableHistoryMapper;

    @Override
    public int delete(Integer id){
        return metaTableHistoryMapper.deleteById(id);
    }

    @Override
    public Integer getLastId(){
        return metaTableHistoryMapper.selectLastId();
    }
}
