package com.demo.tableversion.service.impl;

import com.demo.tableversion.entity.MetaTableVersionHistory;
import com.demo.tableversion.mapper.MetaTableVersionHistoryMapper;
import com.demo.tableversion.mapper.MysqlBaseMapper;
import com.demo.tableversion.service.IMetaTableVersionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MetaTableVersionHistoryServiceImpl implements IMetaTableVersionHistoryService {

    @Autowired
    private MetaTableVersionHistoryMapper metaTableVersionHistoryMapper;

    @Override
    public int save(MetaTableVersionHistory metaTableVersionHistory){
        return metaTableVersionHistoryMapper.insert(metaTableVersionHistory);
    }

    @Override
    public Integer selectLastId() {
        return null;
    }
}
