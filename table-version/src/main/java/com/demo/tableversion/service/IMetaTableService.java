package com.demo.tableversion.service;

import com.demo.tableversion.entity.MetaTable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.tableversion.entity.MetaTableHistory;

import java.util.List;

/**
 * <p>
 * 表元数据 服务类
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface IMetaTableService {

    int saveMetaTable(MetaTable metaTable);
    MetaTable findOne(Integer id);
    int modifyMetaTable(MetaTable metaTable);

    void release(Integer id);

    void delete(Integer id);

    void delete2(Integer id);

    int restoreVersion(MetaTable metaTable);

    List<String> listVersion(Integer id);

    List<MetaTableHistory> listMetaTableHistory(Integer id);

    Integer selectLastId();
}
