package com.demo.tableversion.service;

import com.demo.tableversion.entity.MetaTableVersionHistory;

public interface IMetaTableVersionHistoryService {
    int save(MetaTableVersionHistory metaTableVersionHistory);

    Integer selectLastId();
}
