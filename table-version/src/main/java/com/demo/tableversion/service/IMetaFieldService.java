package com.demo.tableversion.service;

import com.demo.tableversion.entity.MetaField;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字段元数据 服务类
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface IMetaFieldService {

    int saveMetaField(MetaField metaField);
    int saveOrUpdateMetaField(MetaField metaField);

    MetaField findOne(Integer id);
}
