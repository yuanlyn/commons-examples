package com.demo.tableversion.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {
    private String message;
    private Integer state;
    private Object result;
    public static Message on(){
        return new Message();
    }
    public static Message onSuccess(){
        return new Message("success",200,null);
    }
    public static Message onFailed(){
        return new Message("failed",400,null);
    }
    public Message ofMessage(String message){
        this.setMessage(message);
        return this;
    }
    public Message ofState(Integer state){
        this.setState(state);
        return this;
    }
    public Message ofResult(Object result){
        this.setResult(result);
        return this;
    }

}
