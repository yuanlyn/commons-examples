package com.demo.tableversion.controller;


import com.demo.tableversion.entity.MetaField;
import com.demo.tableversion.model.MetaFieldPara;
import com.demo.tableversion.service.IMetaFieldService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字段元数据 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@RestController
@RequestMapping("/field")
public class MetaFieldController {

    @Autowired
    private IMetaFieldService metaFieldService;
    @RequestMapping(value = "/save", method = RequestMethod.GET)
    @ResponseBody
    public Message save(@RequestParam MetaFieldPara metaFieldPara){
        MetaField metaField = new MetaField();
        BeanUtils.copyProperties(metaFieldPara,metaField);
        metaField.setReleaseStat("0");
        if(metaFieldPara.getId() == null){
            metaField.setVersion(MetaTableVersion.NEW);
        }else {
            MetaVersionUtil.incVersion(metaField);
        }
        metaField.setDeleteFlag("0");

        metaFieldService.saveOrUpdateMetaField(metaField);
        return Message.onSuccess();
    }

    @RequestMapping(value = "/modify", method = RequestMethod.GET)
    @ResponseBody
    public Message modify(@RequestParam MetaFieldPara metaFieldPara){
        if(metaFieldPara.getId() == null)return Message.onFailed().ofMessage("缺少唯一主键");
        MetaField metaField = new MetaField();
        BeanUtils.copyProperties(metaFieldPara,metaField);
        metaField.setReleaseStat("0");
        MetaVersionUtil.incVersion(metaField);
        metaField.setDeleteFlag("0");

        metaFieldService.saveOrUpdateMetaField(metaField);
        return Message.onSuccess();
    }
}

