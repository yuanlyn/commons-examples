package com.demo.tableversion.controller;


import com.demo.tableversion.common.Version;
import com.demo.tableversion.entity.MetaField;
import com.demo.tableversion.entity.MetaTable;
import com.demo.tableversion.entity.MetaTableHistory;
import com.demo.tableversion.model.MetaFieldPara;
import com.demo.tableversion.service.IMetaFieldService;
import com.demo.tableversion.service.IMetaTableService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 表元数据 前端控制器
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
@RestController
@RequestMapping("/table")
public class MetaTableController {

    @Autowired
    private IMetaTableService metaTableService;
    @Autowired
    private IMetaFieldService metaFieldService;

    @Transactional
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public Message save(String tableName,@RequestBody(required=false) List<MetaFieldPara> metaFieldList){
        MetaTable metaTable = new MetaTable();
        metaTable.setTablename(tableName);
        metaTable.setDeleteFlag("0");
        metaTable.setReleaseStat("0");
        metaTable.setVersion(MetaTableVersion.NEW);
        int saved = metaTableService.saveMetaTable(metaTable);
        if(saved > 0){
            if(metaFieldList != null && metaFieldList.size() > 0){
                for (MetaFieldPara metaFieldPara : metaFieldList) {
                    MetaField metaField = new MetaField();
                    BeanUtils.copyProperties(metaFieldPara,metaField);
                    metaField.setTableId(metaTable.getId());
                    metaField.setReleaseStat("0");
                    metaField.setVersion(MetaTableVersion.NEW);
                    metaField.setDeleteFlag("0");
                    metaFieldService.saveMetaField(metaField);
                }
            }
        }

        return Message.onSuccess();
    }

    @Transactional
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    @ResponseBody
    public Message modify(Integer id,String tableName,@RequestParam(required=false) List<MetaFieldPara> metaFieldList){
        MetaTable old = metaTableService.findOne(id);
        if(old == null){
            return Message.onFailed().ofMessage("未找到数据");
        }
        if(old.getDeleteFlag().equalsIgnoreCase("1")){
            return Message.onFailed().ofMessage("已删除数据不可修改");
        }

        MetaVersionUtil.incVersion(old);
        old.setTablename(tableName);
        old.setReleaseStat("0");
        int updated = metaTableService.modifyMetaTable(old);
        if(updated > 0){
            if(metaFieldList!= null && metaFieldList.size() > 0){
                for (MetaFieldPara metaFieldPara : metaFieldList) {
                    MetaField metaField = new MetaField();
                    BeanUtils.copyProperties(metaFieldPara,metaField);
                    metaField.setTableId(old.getId());
                    metaField.setReleaseStat("0");
                    if(metaFieldPara.getId() == null){
                        metaField.setVersion(MetaTableVersion.NEW);
                    }else {
                        MetaVersionUtil.incVersion(metaField);
                    }
                    metaField.setDeleteFlag("0");
                    metaFieldService.saveOrUpdateMetaField(metaField);

                }
            }
        }else {
            return Message.onSuccess().ofMessage("未找到数据").ofResult(updated);
        }

        return Message.onSuccess().ofMessage("修改成功");
    }

    @RequestMapping(value = "/release",method = RequestMethod.GET)
    @ResponseBody
    public Message release(@RequestParam(value = "id") Integer id){
        metaTableService.release(id);
        return Message.onSuccess();
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    @ResponseBody
    public Message delete(@RequestParam(value = "id") Integer id){
        metaTableService.delete(id);
        return Message.onSuccess();
    }

    @RequestMapping(value = "/rolling_back",method = RequestMethod.GET)
    @ResponseBody
    public Message rolling_back(@RequestParam Integer id,@RequestParam String version){
        List<MetaTableHistory> metaTableHistories = metaTableService.listMetaTableHistory(id);

        for (MetaTableHistory metaTableHistory : metaTableHistories) {
            String[] split = metaTableHistory.getVersion().split("\\.");
            Version version1 = new Version(split[0], split[1]);
            String[] split2 = version.split("\\.");
            Version version2 = new Version(split2[0], split2[1]);
            if(version1.equals(version2)){
                MetaTable metaTable = new MetaTable();
                BeanUtils.copyProperties(metaTableHistory,metaTable);
                metaTable.setId(id);
                metaTable.setVersion(version);
                int restored = metaTableService.restoreVersion(metaTable);
                if(restored > 0){

                }


                break;
            }
        }



        return Message.onSuccess();
    }
}

