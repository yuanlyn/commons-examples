package com.demo.tableversion.controller;

import com.demo.tableversion.entity.MetaField;
import com.demo.tableversion.entity.MetaTable;

public class MetaVersionUtil {
    public static void incVersion(MetaTable metaTable){
        String version = metaTable.getVersion();
        String[] split = version.split("\\.");
        if("0".equals(metaTable.getReleaseStat())){
            metaTable.setVersion(split[0]+"."+(Integer.valueOf(split[1]).intValue()+1));

        }else {
            metaTable.setVersion((Integer.valueOf(split[0])+1)+"."+"0");

        }

    }

    public static void incVersion(MetaField metaField){
        String version = metaField.getVersion();
        String[] split = version.split("\\.");
        if("0".equals(metaField.getReleaseStat())){
            metaField.setVersion(split[0]+"."+(Integer.valueOf(split[1]).intValue()+1));

        }else {
            metaField.setVersion((Integer.valueOf(split[0])+1)+"."+"0");
        }

    }

}
