package com.demo.tableversion.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class MetaFieldPara implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    /**
     * table_id
     */
    private Integer tableId;

    private String fieldname;

    private String fieldtype;

    private String fieldlength;

    private String comment;




}
