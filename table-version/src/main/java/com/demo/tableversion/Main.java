package com.demo.tableversion;

import org.flywaydb.core.Flyway;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@MapperScan("com.demo.tableversion.mapper")
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Main.class, args);

        Flyway bean = context.getBean(Flyway.class);
        bean.migrate();
    }
    /*
    暂时留在这，应该不用配置flyway的初始化才对
     */
    @Bean
    public Flyway initFlyway(DataSource datasource){
        Flyway flyway = Flyway.configure()
                //.dataSource("jdbc:mysql://192.168.150.133:3306/table_version", "table_version", "123456")
                .dataSource(datasource)
                .load();
//        flyway.migrate();
        flyway.repair();
        return  flyway;
    }
}
