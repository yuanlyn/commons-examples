package com.demo.tableversion.common;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class Version {
    private String big;
    private String small;
    public Version(String s, String s1) {
        this.big = s;
        this.small = s1;
    }

}
