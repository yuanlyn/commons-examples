package com.demo.tableversion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.tableversion.entity.MetaTableVersionHistory;

public interface MetaTableVersionHistoryMapper extends BaseMapper<MetaTableVersionHistory>,MysqlBaseMapper{


}
