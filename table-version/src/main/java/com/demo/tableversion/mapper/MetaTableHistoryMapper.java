package com.demo.tableversion.mapper;

import com.demo.tableversion.entity.MetaTableHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表元数据 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface MetaTableHistoryMapper extends BaseMapper<MetaTableHistory> ,MysqlBaseMapper{

}
