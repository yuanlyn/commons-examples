package com.demo.tableversion.mapper;

import com.demo.tableversion.entity.MetaModifyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表/字段元数据修改历史 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface MetaModifyRecordMapper extends BaseMapper<MetaModifyRecord> {

}
