package com.demo.tableversion.mapper;

import com.demo.tableversion.entity.MetaField;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字段元数据 Mapper 接口
 * </p>
 *
 * @author yl
 * @since 2021-07-16
 */
public interface MetaFieldMapper extends BaseMapper<MetaField>,MysqlBaseMapper {

}
