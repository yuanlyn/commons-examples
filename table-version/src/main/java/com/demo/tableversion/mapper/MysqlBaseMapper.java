package com.demo.tableversion.mapper;

import org.apache.ibatis.annotations.Select;

public interface MysqlBaseMapper {
    @Select(value = {"select LAST_INSERT_ID()"})
    public Integer selectLastId();
}
