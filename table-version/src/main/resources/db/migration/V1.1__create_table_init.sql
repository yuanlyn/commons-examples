CREATE TABLE `meta_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tableName` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `release_stat` varchar(255) DEFAULT NULL,
  `delete_flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表元数据';