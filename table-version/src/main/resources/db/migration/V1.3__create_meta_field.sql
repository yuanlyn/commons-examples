CREATE TABLE `meta_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_id` int(11) NOT NULL  COMMENT 'table_id',
  `fieldName` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fieldType` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fieldLength` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `release_stat` varchar(255) DEFAULT NULL,
  `delete_flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段元数据';

CREATE TABLE `meta_field_history` (
  `id` int(11) NOT NULL COMMENT 'id',
  `table_id` int(11) NOT NULL  COMMENT 'table_id',
  `fieldName` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fieldType` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `fieldLength` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `release_stat` varchar(255) DEFAULT NULL,
  `delete_flag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字段元数据';