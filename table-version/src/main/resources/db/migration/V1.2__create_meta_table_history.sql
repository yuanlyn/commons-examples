CREATE TABLE `meta_table_history` (
  `id` int(11) NOT NULL COMMENT 'id',
  `tableName` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `release_stat` varchar(255) DEFAULT NULL,
  `delete_flag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表元数据';