CREATE TABLE `meta_table_version_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_id` int(11) NOT NULL  COMMENT 'table_id',
  `tableName` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `create_tm` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表版本记录数据';