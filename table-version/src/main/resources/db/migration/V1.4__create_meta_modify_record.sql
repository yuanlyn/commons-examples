CREATE TABLE `meta_modify_record` (
  `record_id` int(11) NOT NULL  COMMENT 'record_id',
  `modify_type` int(4) NOT NULL  COMMENT 'modify_type,1:table,2:field',
  `modify_operation_type` int(4) COMMENT '修改类型（0：新增，1：修改，2：删除)',
  `modify_record_id` int(11) NOT NULL  COMMENT 'table/field的id',
  `old_value` varchar(255) NOT NULL  COMMENT 'old value',
  `new_value` varchar(255) NOT NULL  COMMENT 'new value',
  `version` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表/字段元数据修改历史';
