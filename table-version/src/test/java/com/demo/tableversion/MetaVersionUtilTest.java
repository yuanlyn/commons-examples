package com.demo.tableversion;


import com.demo.tableversion.controller.MetaVersionUtil;
import com.demo.tableversion.entity.MetaTable;
import org.junit.Test;

public class MetaVersionUtilTest {

    @Test
    public void  incVersinTest(){

        MetaTable metatable = new MetaTable();
        metatable.setVersion("1.0");
        MetaVersionUtil.incVersion(metatable);

        System.out.println(metatable.getVersion());
    }
}
