package com.demo.tableversion;

import com.demo.tableversion.entity.MetaTable;
import com.demo.tableversion.service.IMetaTableHistoryService;
import com.demo.tableversion.service.IMetaTableService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {com.demo.tableversion.Main.class})
@RunWith(value = SpringJUnit4ClassRunner.class)
public class MetaTableUpsertAspectTest {

    @Autowired
    private IMetaTableService metaTableService;
    @Autowired
    private IMetaTableHistoryService metaTableHistoryService;
    private MetaTable metaTable;
    @Before
    public void before(){
        this.metaTable = new MetaTable();
        metaTable.setTablename("testttt");
        metaTable.setVersion("2.0");
        metaTable.setDeleteFlag("0");
        metaTable.setReleaseStat("0");
    }
    @Test
    public void test1(){
        metaTableService.saveMetaTable(metaTable);

    }
    @After
    public void after(){
        metaTableService.delete2(metaTable.getId());
        metaTableHistoryService.delete(metaTableHistoryService.getLastId());
    }
}
