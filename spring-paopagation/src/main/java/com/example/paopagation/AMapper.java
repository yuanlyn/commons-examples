package com.example.paopagation;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AMapper {
    @Insert("insert into A values(null)")
    public int insert_A();
}
