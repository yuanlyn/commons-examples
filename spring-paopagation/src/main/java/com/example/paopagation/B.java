package com.example.paopagation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
@Qualifier("b")
public class B {

    @Autowired
    private BMapper mapper;
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = RuntimeException.class)
    public void required() throws IOException {
        mapper.insert_B();
//        try {
//            throw new RuntimeException("RuntimeException");
//
//        }catch (Exception e){
//
//        }
        throw new RuntimeException("RuntimeException");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW,rollbackFor = RuntimeException.class)
    public void required_new(){
        mapper.insert_B();
    }

    @Transactional(propagation = Propagation.NESTED,rollbackFor = RuntimeException.class)
    public void nested(){
        mapper.insert_B();
        throw new RuntimeException("RuntimeException");
    }

    @Transactional(propagation = Propagation.SUPPORTS,rollbackFor = RuntimeException.class)
    public void supportes(){
        mapper.insert_B();
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED,rollbackFor = RuntimeException.class)
    public void not_supported(){
        mapper.insert_B();
    }

    @Transactional(propagation = Propagation.MANDATORY,rollbackFor = RuntimeException.class)
    public void mandatory(){
        mapper.insert_B();
    }

    @Transactional(propagation = Propagation.NEVER,rollbackFor = RuntimeException.class)
    public void never(){
        mapper.insert_B();
    }
}
