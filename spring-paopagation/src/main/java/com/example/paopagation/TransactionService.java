package com.example.paopagation;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Qualifier("transactionService")
public class TransactionService implements BeanFactoryAware {

    BeanFactory beanFactory;
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    public  void invoke(){
        A a = (A) this.beanFactory.getBean("a");
        try {
            a.required_required();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            a.required_required_new();
//
//        }catch (Exception e) {
//            e.printStackTrace();
//        }

//        try {
//            a.required__nested();
//
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
