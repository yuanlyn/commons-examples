package com.example.paopagation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringPaopagationApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpringPaopagationApplication.class, args);

		TransactionService service = (TransactionService) context.getBeanFactory().getBean("transactionService");
		service.invoke();
	}

}
