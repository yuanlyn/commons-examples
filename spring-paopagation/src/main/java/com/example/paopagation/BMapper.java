package com.example.paopagation;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BMapper {

    @Insert("insert into B values(null)")
    public int insert_B();
}
