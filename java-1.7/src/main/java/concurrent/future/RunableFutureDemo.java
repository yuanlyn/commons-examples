package concurrent.future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class RunableFutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

//        runableFuture();
        callableFuture();
    }

    private static void runableFuture() throws InterruptedException, ExecutionException {
        String result= "this is result";
        FutureTask futureTask = new FutureTask(new Runnable() {
            @Override
            public void run() {

            }
        },result);
        new Thread(futureTask).start();
        System.out.println(futureTask.isDone());
        System.out.println(futureTask.get());
        System.out.println(futureTask.isDone());
    }

    private static void callableFuture() throws ExecutionException, InterruptedException {
        FutureTask task = new FutureTask(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "call success";
            }
        });
        new Thread(task).start();
        System.out.println(task.isDone());
        System.out.println(task.get());
        System.out.println(task.isDone());

        new Thread().start();
    }
}
