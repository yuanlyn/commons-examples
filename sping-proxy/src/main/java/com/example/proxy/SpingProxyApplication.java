package com.example.proxy;

import com.example.proxy.sevice.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@SpringBootApplication
public class SpingProxyApplication {

	@Autowired
	DemoService demoService;
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpingProxyApplication.class, args);
		Map<String, Object> beans = context.getBeansWithAnnotation(RequestMapping.class);
		for(Map.Entry<String,Object> entry : beans.entrySet()){
//			System.out.println(entry.getKey() + " === " + entry.getValue().getClass().toGenericString());
			Class<? extends Object> clazz = entry.getValue().getClass();
			if(!clazz.getPackage().getName().startsWith("com.example.proxy")){
				continue;
			}
			RequestMapping ann = AnnotationUtils.findAnnotation(clazz, RequestMapping.class);
			System.out.println(clazz.getClass());
			System.out.println(ann.getClass());
		}

	}




}
