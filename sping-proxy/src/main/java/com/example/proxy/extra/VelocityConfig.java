package com.example.proxy.extra;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VelocityConfig {
    @Bean
    public VelocityEngine get(){
        return new VelocityEngine();
    }
}
