package com.example.proxy.extra;

public class MappingClass {
    private String oldValue;
    private String newValue;
    private char changed;

    public MappingClass(String oldValue, String newValue, char changed) {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.changed = changed;
    }

    public MappingClass() {
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public char getChanged() {
        return changed;
    }

    public void setChanged(char changed) {
        this.changed = changed;
    }
}
