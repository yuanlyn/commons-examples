package com.example.proxy.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hello")
@Qualifier("helloController")
public class MyControlller {

    @RequestMapping("/hi")
    public String hello(){
        return "hello world!";
    }
}
