package com.example.proxy.sevice;

import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService {

    public void hello(){
        System.out.println("hello");
    }
}
