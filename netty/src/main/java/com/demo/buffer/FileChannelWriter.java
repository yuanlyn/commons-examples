package com.demo.buffer;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

public class FileChannelWriter {
    public static void main(String[] args) throws IOException {
        RandomAccessFile file = new RandomAccessFile(new File("D:\\notebook\\nb\\chars.txt"),"rw");
        FileChannel channel = file.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(3);
        byte[] bytes = new byte[]{'a','b','c','c','d','e','f','g','h','i','j','k'};
        int i = 0;
        while (i < bytes.length){
            while (byteBuffer.hasRemaining()){
                byteBuffer.put(bytes[i++]);
            }
            byteBuffer.flip();
            channel.write(byteBuffer);
            byteBuffer.clear();
        }


        channel.close();
    }
}
