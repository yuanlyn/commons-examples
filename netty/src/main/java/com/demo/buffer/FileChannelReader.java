package com.demo.buffer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelReader {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(new File("D:\\notebook\\nb\\chars.txt"));
        FileChannel channel = file.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);
        while (channel.read(byteBuffer) > 0){
            byteBuffer.flip();
            while (byteBuffer.remaining()>0){
                byte b = byteBuffer.get();
                System.out.println((char)b);
            }
            byteBuffer.clear();
        }

        channel.close();
        file.close();

    }
}
