package com.demo.netty.bytebuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

public class PooledByteBufTest {
    public static void main(String[] args) {
        PooledByteBufAllocator alloc = PooledByteBufAllocator.DEFAULT;
        System.out.println( alloc.isDirectBufferPooled());
        ByteBuf buf = alloc.directBuffer();
        buf.writeByte(123);
        buf.release();
    }
}
