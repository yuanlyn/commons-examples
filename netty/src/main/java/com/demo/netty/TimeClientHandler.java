package com.demo.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.Date;

public class TimeClientHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx,Object msg) throws Exception {
        ByteBuf byteBuffer = (ByteBuf)msg;
        try{
            long lnow = (byteBuffer.readUnsignedInt() - 2208988800L) * 1000L;
            System.out.println(new Date(lnow));
            ctx.close();
        }catch (Exception e){

        }finally {
            byteBuffer.release();

        }
    }
}
