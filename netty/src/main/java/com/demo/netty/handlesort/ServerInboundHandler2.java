package com.demo.netty.handlesort;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ServerInboundHandler2 extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        System.out.println("handler 2 ,channel registed");
        ByteBuf byteBuf = ctx.alloc().buffer(7);
        byteBuf.writeBytes("i got you".getBytes() );
        ctx.channel().writeAndFlush(byteBuf);
        ctx.fireChannelRegistered();
    }
    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        System.out.println("receive connect request : " + ctx.channel().id());
        ByteBuf byteBuf = ctx.alloc().buffer(7);
        byteBuf.writeBytes("welcome".getBytes() );
        ctx.channel().writeAndFlush(byteBuf);
        ctx.fireChannelActive();
    }
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("channelInbound 2");
        ctx.fireChannelRead(msg);
    }
}
