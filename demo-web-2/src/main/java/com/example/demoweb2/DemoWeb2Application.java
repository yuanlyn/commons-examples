package com.example.demoweb2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SpringBootApplication
@RestController
@RequestMapping("/app2")
public class DemoWeb2Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoWeb2Application.class, args);
    }

    @RequestMapping("/mapping2")
    public void mapping1(HttpServletRequest request, HttpServletResponse response){
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            Cookie c = cookies[i];
            System.out.println(c.getName()+":"+c.getValue());
        }
    }
}
