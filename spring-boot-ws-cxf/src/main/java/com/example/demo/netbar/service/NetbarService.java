package com.example.demo.netbar.service;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "NetbarService",  //@WebService：将该接口发布成WebService服务器
        targetNamespace = "http://service.netbar.demo.example.com")   //命名空间为包名得倒叙
public interface NetbarService {
    @WebMethod
    String sayHello(@WebParam(name = "userName") String name);
}