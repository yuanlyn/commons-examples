package com.example.demo.netbar.service;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class cxfConfig {
    @Autowired
    private Bus bus;
    @Autowired
    private NetbarService netbarServices;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus,netbarServices);
        endpoint.publish("/NetbarService");//接口发布在 /NetbarServices 目录下
        return endpoint;
    }

}