package com.example.demo.netbar.service;


import org.springframework.stereotype.Component;

import javax.jws.WebService;

@WebService(serviceName = "NetbarService"//服务名
        ,targetNamespace = "http://service.netbar.demo.example.com"//报名倒叙，并且和接口定义保持一致
        ,endpointInterface = "com.example.demo.netbar.service.NetbarService")//包名
@Component
public class NetbarServicesImpl implements NetbarService{
    @Override
    public String sayHello(String name) {
        return "hello" + name;
    }
}