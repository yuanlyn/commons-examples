package com.example.demo.test;

import com.example.demo.netbar.service.NetbarService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;

import javax.xml.namespace.QName;

public class test {

    /**
     * 方式1.代理类工厂的方式,需要拿到对方的接口
     */

    @Test
    public void cl1() {
        try {
            // 接口地址
            String address = "http://localhost:8080/services/NetbarService?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean =  new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(NetbarService.class);
            // 创建一个代理接口实现
            NetbarService cs = (NetbarService) jaxWsProxyFactoryBean.create();
            // 数据准备
            String userName = "micheal Li";
            // 调用代理接口的方法调用并返回结果
            String result = cs.sayHello(userName);
            System.out.println("返回结果:" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    /**
     * 动态调用方式
     */
    @Test
    public void cl2() {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/services/NetbarService?wsdl");

        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("sayHello", "micheal Li");
            System.out.println("返回数据:" + objects[0]);

//            objects = client.invoke(QName.valueOf("http://localhost:8080/services/NetbarService?wsdl","sayHello"), "chen");
//            System.out.println("返回数据:" + objects[0]);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }
}
