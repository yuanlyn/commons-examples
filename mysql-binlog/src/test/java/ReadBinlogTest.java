import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.BinaryLogFileReader;
import com.github.shyiko.mysql.binlog.event.*;
import com.github.shyiko.mysql.binlog.event.deserialization.ChecksumType;
import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ReadBinlogTest {
    @Test
    public void readbinlogfile() throws IOException {
        String filePath = "src/test/resources/mysql-bin.000001";
        File binlogFile = new File(filePath);
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setChecksumType(ChecksumType.CRC32);
        BinaryLogFileReader reader = new BinaryLogFileReader(binlogFile,
                eventDeserializer);
        try {
            // 准备写入的文件名称
            /*
             * File f1 = new File("D:\\mysql-bin.000845.sql"); if
             * (f1.exists()==false){ f1.getParentFile().mkdirs(); }
             */
            FileOutputStream fos = new FileOutputStream(
                    "./mysql-bin.000001.sql", true);
            for (Event event; (event = reader.readEvent()) != null;) {
                System.out.println(event.toString());

                // 把数据写入到输出流
                fos.write(event.toString().getBytes());
            }
            // 关闭输出流
            fos.close();
            System.out.println("输入完成");
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Test
    public void readbinlogstream() throws IOException {
        BinaryLogClient client = new BinaryLogClient("192.168.150.128", 3306, "root", "123456");
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setCompatibilityMode(
                EventDeserializer.CompatibilityMode.DATE_AND_TIME_AS_LONG,
                EventDeserializer.CompatibilityMode.CHAR_AND_BINARY_AS_BYTE_ARRAY
        );

        Map<Long,String> tableIdMap = new HashMap<>();

        client.setEventDeserializer(eventDeserializer);
        client.setBinlogFilename("mysql-bin.000001");
        client.setBinlogPosition(4);
        client.registerEventListener(new BinaryLogClient.EventListener() {

            @Override
            public void onEvent(Event event) {
                //System.out.println(event.toString());
                EventData data = event.getData();
                EventHeader header = event.getHeader();
                EventType eventType = header.getEventType();
                switch (eventType){
                    case TABLE_MAP:
                        TableMapEventData tableData = (TableMapEventData)data;
                        tableIdMap.put(tableData.getTableId(),tableData.getTable());
                        break;
                    case EXT_WRITE_ROWS:
                        WriteRowsEventData rowData = (WriteRowsEventData)data;
                        List<Serializable[]> rows = rowData.getRows();
                        for (Serializable[] row : rows) {
                            List<Object> collect = Arrays.stream(row).map(x -> {
                                Class<? extends Serializable> aClass = x.getClass();
                                switch (aClass.getName()){
                                    case "java.lang.Integer":
                                        return Integer.valueOf(x.toString());
                                    case "java.lang.Long":
                                        return Long.valueOf(x.toString());
                                    case "[B":
                                        byte[] bb = (byte[])x;
                                        String s = new String(bb, 0, bb.length, Charset.forName("utf-8"));
                                        return s;
                                }
                                return x.toString();

                            }).collect(Collectors.toList());
                            System.out.println("ext write table : " + tableIdMap.get(rowData.getTableId())
                                    + ", data : " + collect);
                        }

                        break;
                    case WRITE_ROWS:
                        System.out.println("write : " + data);
                        break;
                }

                EventHeaderV4 eventHeaderV4 = (EventHeaderV4) header;
                long position = eventHeaderV4.getPosition();
                System.out.println("position : " + position);

            }
        });

        client.registerLifecycleListener(new BinaryLogClient.LifecycleListener() {
            @Override
            public void onConnect(BinaryLogClient client) {

            }

            @Override
            public void onCommunicationFailure(BinaryLogClient client, Exception ex) {

            }

            @Override
            public void onEventDeserializationFailure(BinaryLogClient client, Exception ex) {

            }

            @Override
            public void onDisconnect(BinaryLogClient client) {
                String binlogFilename = client.getBinlogFilename();
                long binlogPosition = client.getBinlogPosition();
                System.out.println("connected end at : " + binlogFilename + ",position : " + binlogPosition);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().sleep(5 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    client.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        client.connect();



    }
}
