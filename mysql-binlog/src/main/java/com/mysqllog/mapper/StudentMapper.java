package com.mysqllog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mysqllog.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
