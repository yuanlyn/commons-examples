package com.mysqllog.auto;

import com.mysqllog.entity.Student;
import com.mysqllog.mapper.StudentMapper;
import jdk.nashorn.internal.runtime.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AutoInsert {
    @Autowired
    private StudentMapper studentMapper;

    @Scheduled(fixedRate = 1000*3)
    public void insert_auto(){
        Student student = new Student();
        student.setName(getName());
        student.setAge(getAge());
        studentMapper.insert(student);

    }

    private Random random = new Random(System.currentTimeMillis());
    private Integer getAge() {

        return random.nextInt(100);
    }

    private String getName(){
        return "mr "+System.currentTimeMillis()/1000;
    }

}
