package com.mysqllog.consumer;


import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
    @KafkaListener(topics = {"demo_test_binlog_student"})
    public void listen(ConsumerRecord<String, String> record) {

        String topic = record.topic();
        String value = record.value();
        System.out.println("consumer value : " + value);
    }
}
