package com.example.hessian.common;

import java.io.IOException;

public interface IGreetingService {

    public String greeting(String name) throws IOException;

}