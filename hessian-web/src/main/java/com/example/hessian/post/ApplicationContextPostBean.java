package com.example.hessian.post;

import com.example.hessian.common.IGreetingService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class ApplicationContextPostBean implements ApplicationContextAware {
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        HessianServiceExporter bean = (HessianServiceExporter) applicationContext.getBean("/greetingService");
        IGreetingService service = (IGreetingService) bean.getService();
        String tom = null;
        try {
            tom = service.greeting("tom");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(tom);

        SimpleUrlHandlerMapping simpleUrlHandlerMapping =
                (SimpleUrlHandlerMapping) applicationContext.getBean("simpleMapping");
        Map<String, ?> urlMap = simpleUrlHandlerMapping.getUrlMap();
        urlMap.forEach((key,value) -> {
            System.out.println(key + "," + value);
        });
    }
}
