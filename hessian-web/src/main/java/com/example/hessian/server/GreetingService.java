package com.example.hessian.server;

import com.example.hessian.common.IGreetingService;
import java.io.IOException;

public class GreetingService implements IGreetingService {

    @Override
    public String greeting(String name) throws IOException {
        return "Welcome ot the Hessian world, " + name;
    }

}