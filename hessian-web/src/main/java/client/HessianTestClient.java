package client;

import com.caucho.hessian.client.HessianProxyFactory;
import com.example.hessian.common.IGreetingService;
import com.example.hessian.server.GreetingService;

import java.io.IOException;
import java.net.MalformedURLException;

public class HessianTestClient {
    public static void main(String[] args) {
        String url = "http://127.0.0.1:8668/web/hessian";
        HessianProxyFactory factory = new HessianProxyFactory();
        IGreetingService service = null;
        try {service = (IGreetingService) factory.create(IGreetingService.class, url);}
        catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();}
        //创建IService接口的实例对象
        String helloWorld = null;
        try {
            helloWorld = service.greeting("tom");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //调用Hessian服务器端的ServiceImpl类中的getUser方法来获取一个User对象
        System.out.println(helloWorld);
    }
}
