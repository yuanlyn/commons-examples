package client;

import com.example.hessian.common.IGreetingService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class HessionSpringClient {
    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext classPathXmlApplicationContext = new
                ClassPathXmlApplicationContext("applicationContext-hessian-client.xml");
        IGreetingService greetingService = (IGreetingService) classPathXmlApplicationContext.getBean("greetingService");
        String tom = greetingService.greeting("tom");
        System.out.println(tom);
    }
}
