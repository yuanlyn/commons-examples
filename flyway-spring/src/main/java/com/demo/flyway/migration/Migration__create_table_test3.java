package com.demo.flyway.migration;

import org.flywaydb.core.api.MigrationVersion;
import org.flywaydb.core.api.migration.Context;
import org.flywaydb.core.api.migration.JavaMigration;
import org.springframework.stereotype.Component;

import java.sql.Statement;

@Component
public class Migration__create_table_test3 implements JavaMigration {
    @Override
    public MigrationVersion getVersion() {
        return MigrationVersion.fromVersion("1.3");
    }

    @Override
    public String getDescription() {
        return "create table test3(`tableName` varchar(20))";
    }

    @Override
    public Integer getChecksum() {
        return 1;
    }

    @Override
    public boolean isUndo() {
        return false;
    }

    @Override
    public boolean canExecuteInTransaction() {
        return false;
    }

    @Override
    public void migrate(Context context) throws Exception {
        Statement statement = context.getConnection().createStatement();
        statement.execute("create table test3(`tableName` varchar(20))");
    }
}
