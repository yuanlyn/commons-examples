package com.demo.flyway;


import com.demo.flyway.migration.Migration__create_table_test3;
import com.demo.flyway.migration.Migration__insert_table_test3;
import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MainApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);

        Flyway flyway = Flyway.configure()
                .dataSource("jdbc:mysql://192.168.150.133:3306/flyway", "flyway", "123456")
                .javaMigrations(
                        run.getBean(Migration__create_table_test3.class)
                        ,run.getBean(Migration__insert_table_test3.class)
                )
                .load();
//        flyway.clean();
        flyway.migrate();
    }
}
