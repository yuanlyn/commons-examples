package com.demo.sync;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

public class SyncProducer {
    public static void main(String[] args) throws Exception {
        //Instantiate with a producer group name.
        DefaultMQProducer producer = new
                DefaultMQProducer("sync_producer_group_test1");
        // Specify name server addresses.
        producer.setNamesrvAddr("localhost:9876");
        //Launch the instance.
        producer.start();
        for (int i = 0; i < 100000; i++) {
            //Create a message instance, specifying topic, tag and message body.
            Message msg = new Message(
                    "sync_TopicTest" /* Topic */,
                    "sync_TagA" /* Tag */,
                    ("sync message : Hello RocketMQ " + i).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            //Call send message to deliver message to one of brokers.
            SendResult sendResult = producer.send(msg);
            System.out.printf("%s%n", sendResult);
            Thread.currentThread().sleep(1000);
        }
        //Shut down once the producer instance is not longer in use.
        producer.shutdown();
    }
}