package com.demo.sync;

class MyThread extends Thread{
    private Runnable runable;
    Long offset;
    public MyThread(){}
    public MyThread( Long offset,Runnable runnable){
        this.offset = offset;
        this.runable = runnable;
    };

    public Runnable getRunable() {
        return runable;
    }

    public void setRunable(Runnable runable) {
        this.runable = runable;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    @Override
    public void run() {
        runable.run();
    }
}