package com.demo;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.protocol.body.ClusterInfo;
import org.apache.rocketmq.common.protocol.body.TopicList;
import org.apache.rocketmq.remoting.exception.RemotingConnectException;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.remoting.exception.RemotingSendRequestException;
import org.apache.rocketmq.remoting.exception.RemotingTimeoutException;
import org.apache.rocketmq.tools.admin.DefaultMQAdminExt;

public class TopicCreator {
    /**
     *
     * @param topicname  主题名称
     * @param queuenum  单个broker下的队列个数
     * @param producerGroup 生产者组，消费者和生产者应该属于同一个组
     * @param brokerNm 创建在哪个broker上
     */
    public static void create(String topicname, int queuenum, String producerGroup, String brokerNm){
        DefaultMQProducer producer = new
                DefaultMQProducer(producerGroup);
        // Specify name server addresses.
        producer.setNamesrvAddr("localhost:9876");
        //Launch the instance.
        try {
            producer.start();
            producer.createTopic(brokerNm,topicname,queuenum);
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        producer.shutdown();
    }
    public static void getBrokerList() throws MQClientException {
        DefaultMQAdminExt defaultMQAdminExt = new DefaultMQAdminExt(1000);
        defaultMQAdminExt.start();
        TopicList topicList = null;
        try {
            topicList = defaultMQAdminExt.fetchAllTopicList();
            topicList.getTopicList().forEach(System.out::println);
            System.out.println("broker :: "+topicList.getBrokerAddr());
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        defaultMQAdminExt.shutdown();
    }

    public static void main(String[] args) {
//        TopicCreator.create("blankTopic2",6,"blankTopicGroup", "9UK2LWH8QG7W4LM");

        try {
            TopicCreator.getBrokerList();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }
}
