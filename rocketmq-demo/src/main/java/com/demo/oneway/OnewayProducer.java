package com.demo.oneway;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.Random;

public class OnewayProducer {
    public static void main(String[] args) throws Exception{
        //Instantiate with a producer group name.
        DefaultMQProducer producer = new DefaultMQProducer("oneway_producer_group");
        // Specify name server addresses.
        producer.setNamesrvAddr("localhost:9876");
        //Launch the instance.
        producer.start();
        /*for (int i = 0; i < 100; i++) {
            //Create a message instance, specifying topic, tag and message body.
            Message msg = new Message("oneway_TopicTest" *//* Topic *//*,
                    "TagA" *//* Tag *//*,
                    ("Hello RocketMQ " +
                            i).getBytes(RemotingHelper.DEFAULT_CHARSET) *//* Message body *//*
            );
            //Call send message to deliver message to one of brokers.
            producer.sendOneway(msg);
        }*/
        Random random = new Random(System.currentTimeMillis());
        for (;;) {
            //Create a message instance, specifying topic, tag and message body.
            Message msg = new Message("oneway_TopicTest" /* Topic */,
                    "TagA" /* Tag */,
                    ("Hello RocketMQ " +
                            random.nextInt(100000)).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            //Call send message to deliver message to one of brokers.
            producer.sendOneway(msg);
            Thread.sleep(1000);
        }
        //Wait for sending to complete
//        Thread.sleep(5000);
//        producer.shutdown();
    }
}