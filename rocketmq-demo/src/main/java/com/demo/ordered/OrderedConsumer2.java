package com.demo.ordered;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

public class OrderedConsumer2 {
    public static void main(String[] args) throws Exception {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("ordered_group");

        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        consumer.setConsumeMessageBatchMaxSize(1);
        consumer.setMessageModel(MessageModel.CLUSTERING);
        consumer.subscribe("ordered_topic", "睡觉 || 吃饭");

        consumer.registerMessageListener(new MyMessageListenerOrderly());

        consumer.start();

        System.out.printf("Consumer Started.%n");
    }
}