package com.demo.ordered;

import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;

import java.nio.charset.Charset;
import java.util.List;

public class MyMessageListenerOrderly implements MessageListenerOrderly {

    /*
    保证单个queue是消费有序的，是因为单个队列始终由一个线程消费，多余的线程将不会进行消费
     */
    @Override
    public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs,
                                               ConsumeOrderlyContext context) {
        for (int i = 0; i < msgs.size(); i++) {
            System.out.printf(Thread.currentThread().getName() + " Receive New Messages: "
                    + new String(msgs.get(i).getBody(), Charset.forName("UTF-8")) + "%n");
        }
        return ConsumeOrderlyStatus.SUCCESS;

    }
}
