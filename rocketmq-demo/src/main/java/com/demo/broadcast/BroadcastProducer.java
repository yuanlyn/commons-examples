package com.demo.broadcast;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

public class BroadcastProducer {
    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        DefaultMQProducer producer = new DefaultMQProducer("broadcast_group");
        producer.setRetryTimesWhenSendAsyncFailed(3);
        producer.setNamesrvAddr("localhost:9876");
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message message = new Message();
            message.setTopic("broadcast_topic");
            message.setKeys("key"+i);
            message.setBody(String.valueOf("hello body "+i).getBytes());
            producer.send(message);
        }
        producer.shutdown();
    }
}
