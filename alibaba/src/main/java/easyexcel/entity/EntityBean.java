package easyexcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

import javax.sql.rowset.BaseRowSet;

public class EntityBean extends BaseRowModel {
    @ExcelProperty(index = 0,value = "实体名称（必填项）")
    private String entNm;
    @ExcelProperty(index = 1,value = "实体等级")
    private String entLvl;
    @ExcelProperty(index = 2,value = "实体描述")
    private String entDesc;
    @ExcelProperty(index = 3,value = "覆盖实体名称")
    private String entNmOverride;

    public String getEntNm() {
        return entNm;
    }

    public void setEntNm(String entNm) {
        this.entNm = entNm;
    }

    public String getEntLvl() {
        return entLvl;
    }

    public void setEntLvl(String entLvl) {
        this.entLvl = entLvl;
    }

    public String getEntDesc() {
        return entDesc;
    }

    public void setEntDesc(String entDesc) {
        this.entDesc = entDesc;
    }

    public String getEntNmOverride() {
        return entNmOverride;
    }

    public void setEntNmOverride(String entNmOverride) {
        this.entNmOverride = entNmOverride;
    }


    @Override
    public String toString() {
        return "EntityBean{" +
                "entNm='" + entNm + '\'' +
                ", entLvl='" + entLvl + '\'' +
                ", entDesc='" + entDesc + '\'' +
                ", entNmOverride='" + entNmOverride + '\'' +
                '}';
    }

    public boolean validate() {
        return  (entNm != null && !"".equals(entNm) && !"null".equals(entNm))
                &&((entLvl != null && !"".equals(entLvl) && !"null".equals(entLvl))
                ||(entDesc != null && !"".equals(entDesc) && !"null".equals(entDesc))
                ||(entNmOverride != null && !"".equals(entNmOverride) && !"null".equals(entNmOverride)))
                ;
    }
}
