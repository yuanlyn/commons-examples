package easyexcel;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.Sheet;
import easyexcel.entity.OldMan;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EasyExcelRead {

    private String filename;
    public EasyExcelRead(String arg) {
        this.filename = arg;
    }

    public static void main(String[] args) {
        EasyExcelRead easyExcelRead = new EasyExcelRead("D:\\git\\commons-examples\\alibaba\\src\\main\\java\\easyexcel\\test.xlsx");
        easyExcelRead.readFile();
    }

    private ThreadLocal<List<OldMan>> threadLocal = new ThreadLocal<>();
    private ThreadLocal<List<OldMan>> threadLocal2 = new ThreadLocal<>();

    private void readFile() {
        try {
            MyListenerImpl listener1 = new MyListenerImpl();
            EasyExcelFactory.readBySax(new FileInputStream(filename),
                    new Sheet(1,1,OldMan.class),
                    listener1);
            EasyExcelFactory.readBySax(new FileInputStream(filename),
                    new Sheet(2,1,OldMan.class),
                    new MyListenerImpl());
            System.out.println("测试是否异步的"+Thread.currentThread().getId());
            for (int i = 0; i < listener1.getThreadLocal().get().size(); i++) {
                OldMan oldMan = (OldMan) listener1.getThreadLocal().get().get(i);
                System.out.println(oldMan.toString());
                System.out.println();
            }

            Map<String, List<OldMan>> x = listener1.getThreadLocal().get().stream().
                    collect(Collectors.groupingBy(oldMan -> oldMan.getName()));

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private class MyListenerImpl  extends AnalysisEventListener {
        private ThreadLocal<List<OldMan> > threadLocal = ThreadLocal.withInitial(() -> {
            return new ArrayList<OldMan>();
        });
        List<OldMan> data = new ArrayList<>();
        @Override
        public void invoke(Object o, AnalysisContext analysisContext) {
            threadLocal.get().add((OldMan)o);
        }

        @Override
        public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//            readAsString();
//            readAsOldMan();
            System.out.println("输出完成"+Thread.currentThread().getId());
//            threadLocal.set(data);
        }

        private void readAsOldMan() {
            for (int i = 0; i < data.size(); i++) {
                OldMan oldMan = (OldMan) data.get(i);
                System.out.println(oldMan.toString());
                System.out.println();
            }
        }

        private void readAsString(){
            for (int i = 0; i < data.size(); i++) {
                List<String> stringList = (List<String>) data.get(i);
                for (int i1 = 0; i1 < stringList.size(); i1++) {
                    System.out.print(stringList.get(i1));
                    System.out.print("\t");
                }
                System.out.println();
            }
        }

        public ThreadLocal<List<OldMan>> getThreadLocal() {
            return threadLocal;
        }

        public void setThreadLocal(ThreadLocal<List<OldMan>> threadLocal) {
            this.threadLocal = threadLocal;
        }
    }
}
