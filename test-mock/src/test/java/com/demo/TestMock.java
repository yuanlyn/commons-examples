package com.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
@RunWith(MockitoJUnitRunner.class)
public class TestMock {


    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @Test
    public void testHttpServletRequest(){
        String abc = request.getParameter("abc");


        System.out.println(abc);
    }

    @Test
    public void testMock(){
        List list = mock(List.class);
        when(list.get(0)).thenReturn(1).thenReturn(2);
        System.out.println(list.get(0));
        System.out.println(list.get(0));
    }

}
